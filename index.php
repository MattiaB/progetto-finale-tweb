<?php

set_include_path(get_include_path() . PATH_SEPARATOR . 'framework' . PATH_SEPARATOR . 'app' . PATH_SEPARATOR . 'app/controllers' . PATH_SEPARATOR . 'app/models' . PATH_SEPARATOR . 'app/helpers');
$CONFIG = array();

function __autoload($class) {
	require ucfirst($class) . '.php';
}

require 'config.php';
require 'routes.php';
require 'helpers.php';
Cache::init();
try {
	Routing::config($config,$route,$inverseRoute);
	$mysql = Record::connect($config["mysqlHost"],$config["mysqlUser"],$config["mysqlPsw"],$config["mysqlDb"],$config["mysqlTables"]);
	if ($mysql) {
		$getData = Routing::matchUri();
		Controller::callController($getData);
		Record::close_connect();

	}
} catch (Exception $exc) {
	?>
<div class="errors">
<p>
<?php echo safe($exc->getMessage()); ?>
</p>
</div>
<?php
try {
	if($exc->getCode() == 1 || $exc->getCode() == 2 || $exc->getCode() == 3){
		require 'dbconfig.php';
	}
} catch (Exception $exc) {
	?>
<div class="errors">
<p>
<
<?php
echo safe($exc->getMessage());
?></p>
</div>
<?php
echo link_to("index","Series Premiere HOME");
}
}
Cache::end();
?>

