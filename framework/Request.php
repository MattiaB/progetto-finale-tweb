<?php

class Request implements ArrayAccess, Iterator{

	private $data = array();

	function __construct($data, $keyUnSafe = array()) {
		foreach ($data as $key => $value) {
			if(!array_key_exists($key, $keyUnSafe) && is_string($value)){
				$value = htmlspecialchars($value);
				$key = htmlspecialchars($key);
			}
			$this->data[$key] = $value;
		}
	}
	function to_http_query($data = null) {
		return static::static_to_http_query($this->data);
	}
	static function static_to_http_query($data) {
		return '?'.http_build_query($data);
	}
	public function isEmpty() {
		return empty($this->data);
	}

	public function __get($name) {
		if (array_key_exists($name, $this->data))
		return $this->data[$name];
		else
		return null;
	}
	public function __set($name, $value) {
		$this->data[$name] = $value;
	}

	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this->data[] = $value;
		} else {
			$this->data[$offset] = $value;
		}
	}

	public function offsetExists($offset) {
		return isset($this->data[$offset]);
	}

	public function offsetUnset($offset) {
		unset($this->data[$offset]);
	}

	public function offsetGet($offset) {
		return isset($this->data[$offset]) ? $this->data[$offset] : null;
	}
	public function rewind() {
		reset($this->data);
	}
	public function current(){
		return current($this->data);
	}
	public function key() {
		return key($this->data);
	}
	public function next() {
		return next($this->data);
	}
	public function valid() {
		$key = key($this->data);
		return ($key !== NULL && $key !== FALSE);
	}
}

?>