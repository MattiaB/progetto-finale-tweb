<?php

class Controller {

	private $view;
	public $session;
	public $get;
	public $post;
	private $data = array();
	public $scripts = array();
	public $css = array();
	public static $beforeFilter = array();
	public $flash;
	private function init($calledMethod) {
		$stringClass = preg_replace("/Controller/", '', get_class($this));
		set_include_path(get_include_path() . PATH_SEPARATOR . "app/views/layouts");
		set_include_path(get_include_path() . PATH_SEPARATOR . "app/views/share");
		set_include_path(get_include_path() . PATH_SEPARATOR . "app/views/$stringClass");
		$this->get = new Request($_GET);
		$this->post = new Request($_POST);
		$this->view = $calledMethod;
		$this->session = new Session();
		$this->flash = new Flash($this->session);
	}
	function session() {

		//var_dump($this->session->id);
	}

	public function __get($name) {
		if (array_key_exists($name, $this->data))
		return $this->data[$name];

	}

	public function __set($name, $value) {
		$this->data[$name] = $value;
	}
	public function whoIam() {
		return get_called_class();
	}
	function view($view = null, $data=null) {
		$this->data += (array)$data;
		foreach ($this->data as $key => $value) {
			${
				$key} = $value;
		}
		if ($view) {
			$this->view = $view;
		}
		if(!$this->isAjax()){
			require "application.html.php";
		}else{
			require "$this->view.html.php";
		}

	}
	function partial($partial, $data=array()) {
		foreach ((array)$data as $key => $value) {
			${
				$key} = $value;
		}
		require "_$partial.html.php";
	}
	function isAjax() {
		return isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"]=="XMLHttpRequest";
	}
	function content($view=null) {
		foreach ($this->data as $key => $value) {
			${
				$key} = $value;
		}
		if ($view)
		require "$view.html.php";
		else
		require "$this->view.html.php";
	}
	function redirectTo($uri) {
		if(is_array($uri)){
			$uri = uri($uri);
		}else{
			if($uri == "index"){
				$uri = 'http://'.Routing::$beginningUri.'index.php';
			}elseif($uri == "referer"){
				$this->callbacks("beforeRedirectToRefer",'redirectTo');
				if($_SERVER["HTTP_REFERER"]){
					$uri=$_SERVER["HTTP_REFERER"];
				}else $uri = 'http://'.Routing::$beginningUri.'index.php';
			}
			else{
				$beginingUri = Routing::$beginningUri;
				$uri = "http://".$beginingUri.$uri;
			}
		}
		$this->flash->save();
		header("Location: $uri");
		exit;
	}
	static public function callController($uri) {
		$array = explode('/', $uri);
		$array = (array)$array;
		$array += array('','');
		$array[0] = ucfirst($array[0]);
		$array[0] = $array[0].'Controller';

		if(!class_exists($array[0]))throw new Exception("Classe inesistente $array[0].");
		$class = $array[0];
		unset($array[0]);
		$obj = new $class;

		$method = $array[1];

		if(!method_exists($obj,$method)){
			$method = 'index';
		}else unset($array[1]);
		if(!is_callable(array($obj, $method)))
		throw new Exception("Il metodo $method della classe $class non esiste.");

		$ReflectionMethod = new ReflectionMethod($class, $method);
		if (!($ReflectionMethod->getNumberOfRequiredParameters() <= count($array)))
		throw new Exception("Il metodo $method della classe $class  richiede più parametri.");

		$obj->init($method);
		$obj->callbacks($class::$beforeFilter,$method,$array);
		call_user_func_array(array($obj, $method),$array);
		//$obj->callbacks("afterFilter");
	}
	protected  function callbacks($arrayFunc,$method) {
		if(is_string($arrayFunc)){
			if(method_exists($this,$arrayFunc)){
				call_user_func_array(array($this, $arrayFunc),func_get_args());
			}
		}else{
			foreach ((array)$arrayFunc as $key => $methodsCall) {
				foreach ($methodsCall as $value) {
					if($value == $method){
						if(method_exists($this,$key)){
							call_user_func_array(array($this, $key),func_get_args());
						}
					}
				}
			}
		}
	}
}
class Flash implements ArrayAccess{
	private $data =array();
	private $session;
	function __construct(&$session) {
		foreach ((array)$session->flash as $key => $value) {
			$this->data[$key] = $value;
		}
		$session->flash = null;
		$this->session = $session;
	}
	public function save() {
		$this->session->flash = $this->data;
	}
	public function isEmpty() {
		return empty($this->data);
	}

	public function __get($name) {
		if (array_key_exists($name, $this->data))
		return $this->data[$name];
		else
		return array();
	}

	public function __set($name, $value) {
		if(!$this->data[$name])
		$this->data[$name]= array();
		$this->data[$name] += (array)$value;
	}

	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this->data[] = $value;
		} else {
			$this->data[$offset] = $value;
		}
	}

	public function offsetExists($offset) {
		return isset($this->data[$offset]);
	}

	public function offsetUnset($offset) {
		unset($this->data[$offset]);
	}

	public function offsetGet($offset) {
		return isset($this->data[$offset]) ? $this->data[$offset] : null;
	}

}

?>
