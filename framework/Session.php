<?php

class Session {

	public $id;
	private $data;
	public function __construct() {
		@session_start();
		$this->id = session_id();
		$this->data = &$_SESSION;
	}

	public function __get($name) {
		if (array_key_exists($name, $this->data))
		return $this->data[$name];
		else
		return null;
	}

	public function __set($name, $value) {
		$this->data[$name] = $value;
	}

	public function insertArray($array) {
		foreach ($array as $key => $value) {
			$_SESSION[$key]= $value;
		}
	}
	public function destroy() {
		session_destroy();
	}



}

?>