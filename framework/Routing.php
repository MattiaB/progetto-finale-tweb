<?php
/*
 // Test Routing
$config["urlPath"]="localhost/progetto_finale/framework/";
$config["urlRoot"]="acdc/index";
require '../app/routes.php';
Routing::config($config, $route,"Routing.php");
var_dump(Routing::$beginningUri);
var_dump(Routing::$getData);
var_dump(Routing::$rootUri);
var_dump(Routing::$route);
var_dump(Routing::matchUri(Routing::$getData));
echo "prova da controller a uri<br>";
var_dump(Routing::getCompleteUri(array("blog","index","2011","12","10")));
var_dump(Routing::getCompleteUri(array("registration","logout")));
*/
class Routing{
	public static $beginningUri = "";
	public static $rootUri = "";
	public static $route = array();
	public static $inverseRoute = array();
	public static $getData = "";
	public static function config($config,$route,$inverseRoute,$indexPhp = "index.php") {
		$completePath = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		$path = preg_replace("/$indexPhp.*/i", '', $completePath);
		if($path != $config["urlPath"]){
			$config["urlPath"] = $path;	
			if (is_writable("app/config.php")){
				$stringFile = "<?php\n";
				foreach ($config as $key => $value) {
					$stringFile .= '$config["' . $key . '"]="' . $value . '";' . "\n";
				}	
				$stringFile .= "\n?>";
				file_put_contents("app/config.php", $stringFile);
			}else{
				throw new Exception("Errore non si hanno i permessi per scrivere sul file app/config.php");
			}
		}
		Routing::$beginningUri = $config["urlPath"];
		Routing::$rootUri = $config["urlRoot"];
		$completePath = str_replace( Routing::$beginningUri,'',$completePath);
		Routing::$getData = preg_replace('/.*?'.$indexPhp.'?[\/]?/i', '', $completePath);// modificato prima c'era anche |.*? per prendere link senza index
		Routing::$route = $route;
		Routing::$inverseRoute = $inverseRoute;
	}
	public static function matchRoute($route,$subject, $callback){
		foreach ($route as $key => $value) {
			if(preg_match('/'.$key.'/i',$subject,$matches)){
				$callback('/'.$key.'/i',$value,$matches);
				break;
			}
		}
	}
	public static function getCompleteUri($getData) {
		if(is_array($getData)){
			$getData = implode('/', $getData);
		}

		$finalGetData = "";
		Routing::matchRoute(Routing::$inverseRoute ,$getData, function ($key,$value,$matches) use($getData,&$finalGetData){
			$finalGetData = preg_replace($key,$value,$getData);
		});

		if($finalGetData == ""){
			Routing::matchRoute(Routing::$route,$getData, function ($key,$value,$matches) use(&$finalGetData){
				$finalGetData = $matches[0];
			});
		}
		if ($finalGetData == ""){
			$finalGetData = $getData;
		}
		return 'http://'.Routing::$beginningUri.'index.php/'.$finalGetData;
	}
	public static function matchUri($uri = null) {

		if(!$uri) $uri = Routing::$getData;
		if(!$uri) $uri = Routing::$rootUri;
		$ris = $uri;
		Routing::matchRoute(Routing::$route,$uri,function ($key,$value) use(&$ris,$uri){
			$ris = preg_replace($key,$value,$uri);
		});
		return $ris;
	}
}
?>
