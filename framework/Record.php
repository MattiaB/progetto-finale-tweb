<?php

class Record implements Serializable{

	private static $link = null;
	private $data = array();
	public $errors = array();
	public  static $num_query = 0;
	public static $validate = array();
	public static $has_many = array();
	public static $has_many_to_many = array();
	public static $has_one = array();
	public static $last_query_count = 0;
	public function __construct($array_assoc = null) {
		$fields = static::describeTable();
		foreach ($fields as $field => $property) {
			$this->{$field} = $property['Default'];
		}
		if($array_assoc){
			foreach ($array_assoc as $key_var => $value_var) {
				$this->{$key_var}  = $value_var;
			}
		}
	}
	/*
	 * Inizializza l'oggetto con i dati di un array associativo
	* dove le key sono le variabili di istanza dell'oggetto
	*/
	public function setData($array_assoc) {
		$array_aux = $this->callbacks("beforeSetData",$array_assoc);
		if($array_aux)
		$array_assoc=$array_aux;
		if($array_assoc){
			foreach ($array_assoc as $key_var => $value_var) {
				$this->{$key_var}  = $value_var;
			}
		}
		$this->callbacks("afterSetData");
	}
	private function callbacks($name)  {
		if(method_exists($this,$name)){
			return call_user_func_array(array($this, $name),func_get_args());
		}return false;
	}
	/*
	 * Restituisce un array di stringhe dove ci sono tutti i campi
	* della tabella
	*/
	static private function describeTable() {
		$table = static::tableName();
		$query = "SHOW COLUMNS FROM `$table`";
		try {
			$fields = Cache::get($query);
		} catch (Exception $e) {
			$result = static::query($query);
			if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_assoc($result)) {
					$fields [$row['Field']] = $row;
				}
			}
			Cache::add($query, $fields);
		}

		return (array)$fields;
	}
	static function create($array_assoc) {
		$obj = new static($array_assoc);
		if($obj->save())return $obj;
		else 			return null;
	}
	static function find_and_create($array_assoc, $cond=null) {
		if(!$cond){
			$elem = static::find($array_assoc,array('first'=>true,'limit'=>'1'));
		}else{
			$elem = static::find($cond,array('first'=>true,'limit'=>'1'));
		}
		if(!$elem){
			$obj =  new static($array_assoc);
			$obj->save();
			return $obj;
		}
		else return $elem;
	}
	static function query($queryString) {
		$result = @mysql_query($queryString);
		Record::$num_query++;
		if (!$result) {
			throw new Exception("Could not run query $queryString: " . mysql_error(),4) ;
		}
		return $result;
	}
	private function has_many($many_class,$pref = 'id_',$is_many_to_many=false) {
		$this_class = get_called_class();
		$many_class = ucfirst($many_class);
		//if($is_many_to_many )
		$many_class = substr($many_class, 0, -1);
		$var =  strtolower($this_class);
		//if (substr($this_class, -1) == 's')	$var =  strtolower($this_class);
		//else 								$var =  strtolower($this_class).'s';
		//var_dump($many_class);
		//var_dump(array($pref.$var=>$this->id));
		return $many_class::find(array($pref.$var=>$this->id));
	}
	private function has_one($one_class,$this_id = 'id_') {
		$class = ucfirst($one_class);
		//if(!class_exists($one_class))
		//$one_class =
		//var_dump($this_id.$one_class);
		if($this_id != 'id_'){
			$field = $this_id;
		}else{
			$field= $this_id.$one_class;
		}
		return $class::find(array('id'=>$this->{$field}),array("first"=>true));
	}
	public function __get($name) {
		if (array_key_exists($name, $this->data))
		return $this->data[$name];
		if(in_array($name,static::$has_many)){
			$this->data[$name] =  $this->has_many($name);
			return $this->data[$name];
		}
		if(in_array($name,static::$has_one)){
			$this->data[$name] = $this->has_one($name);
			return $this->data[$name];
		}
		if(array_key_exists($name,static::$has_many_to_many)){
			if(is_array(static::$has_many_to_many[$name])){
				$many = static::$has_many_to_many[$name][1];
				$to_many = static::$has_many_to_many[$name][0];
			}else{
				$many = static::$has_many_to_many[$name];
				$to_many =$name;
			}
			$array = $this->has_many($many,'id_',true);
			$this->data[$name] = array();
			$to_many = substr($to_many, 0, -1);
			foreach ((array)$array  as $one_of_many) {
				$one= $one_of_many->has_one($to_many,"id_$to_many"); // Correggere deve restituire un array di user no un singolo user
				$this->data[$name] []= $one;
			}
			return $this->data[$name];
		}
		return null;
	}

	public function __set($name, $value) {
		if ($name == 'id')
		$value = intval($value);

		$this->data[$name] = $value;
	}
	/*
	 * Da correggere e redere più dinamica con automi
	*/
	static function __callstatic($name, $param) {
		$value = explode('_', $name);
		$cond = array();
		$i = 0;
		$option = array();
		if (current($value) == 'find') {
			$aux = next($value);
			if ($aux == 'first') {
				$option["first"]= true;
				$aux = next($value);
			}
			if ($aux == 'by') {
				while ($field = next($value)) {
					if ($field == 'and')
					continue;
					$cond[$field] = $param[$i];
					$i++;
				}
			}
		}
		return static::find($cond, $option );
	}

	public function __toString() {
		$ris = '';
		foreach ($this as $key => $value) {
			print "$key => $value | ";
		}
		return $ris;
	}
	/*
	 * Selezione il db scritto nella varibile config e lancia una eccezione con codice 2 se non esiste
	*/
	static public function select_db($db) {
		$db_selected = mysql_select_db($db, Record::$link);
		if (!$db_selected) {
			throw new Exception("Non il database non esiste",2);
		}
		return true;
	}
	/*
	 * Controlla che le tabelle scritte nella variabile config siano tutte presenti ne database
	* altrimenti restituisce una eccezione con codice 3
	*/
	static public function controlTable($tables, $db) {
		if($tables != ''){
			$tables = explode(",", $tables);
			$query = "SHOW TABLES FROM $db";
			$db_tables = array();
			try {
				$db_tables = Cache::get($query);
			} catch (Exception $e) {
				$result = static::query($query);
				while($row = mysql_fetch_row($result)){
					$db_tables []= $row[0];
				}
				Cache::add($query, $db_tables);
			}
			if($db_tables){
				$ris = array_diff($tables,$db_tables);
				if($ris){
					throw new Exception("Non mancano delle tabelle nel database",3);
				}
			}else{
				throw new Exception("Non c'è nessuna tabella nel database",3);
			}
		}
		return true;
	}
	/*
	 * Connessione al database può restituire una eccezione con codice 1
	*/
	static public function connect($host, $user, $password, $db, $tables =null) {
		if (!Record::$link) {
			ini_set('mysql.connect_timeout', 2);
			Record::$link = @mysql_connect($host, $user, $password);
			if (!Record::$link) {
				throw new Exception("Non e' possibile connettersi al db: ".mysql_error(),1);
			}
			if(Record::select_db($db)){
				return Record::controlTable((string)$tables,$db);
			}
		}
	}
	/*
	 * Chiude la connessione con il DB
	*/
	static public function close_connect() {
		return mysql_close(Record::$link);
		Record::$link = null;
	}
	/*
	 * Restituisce il nome della della tabella riferita alla classe chiamante
	*/
	static private function tableName() {
		$class = get_called_class();
		return get_called_class() . 's';
	}
	/*
	 * Restuisce tutti i record della tabella chiamante
	*/
	static public function all($option=null) {
		return static::find(null, $option);
	}
	/*
	 * Conta tutte le righe della tabella
	*/
	static public function count($cond, $option = null) {
		$option  = (array)$option + array("select"=>"COUNT(*)");
		$query = static::buildQuery($cond,$option);
		try {
			$total = Cache::get($query);
		} catch (Exception $e) {
			$result = static::query($query);
			$total = mysql_fetch_row($result);
			Cache::add($query, $total);
		}

		return intval($total[0]);
	}
	static public function countAll() {
		return static::count(null,null);
	}
	/*
	 * Rende sicure le stringhe per proteggersi dall'SQL Injection
	*/
	static public function safe($string) {
		return mysql_real_escape_string($string, Record::$link);
	}
	/*
	 * Restituisce l'ultimo id della tabella
	*/
	static public function lastId() {
		$id = null;
		$table = static::tableName();
		$query = "SHOW TABLE STATUS LIKE '$table'";
		try {
			$row = Cache::get($query);
		} catch (Exception $e) {
			$row=null;
			$result = static::query($query);
			if ($result) {
				$row = mysql_fetch_assoc($result);
			}
			Cache::add($query, $row);
		}
		if ($row) {
			$id = $row['Auto_increment']-1;
		}
		return $id;
	}
	/*
	 * Restituisce il primo id della tabella
	*/
	static public function firstId() {
		$id = null;
		$table = static::tableName();
		$query = static::select(array("MIN(id) AS min"));
		try {
			$row = Cache::get($query);
		} catch (Exception $e) {
			$row=null;
			$result = static::query($query);
			if ($result) {
				$row = mysql_fetch_assoc($result);
			}
			Cache::add($query, $row);
		}

		if ($row) {
			$id = $row['min'];
		}
		return $id;
	}
	/*
	 * Aggiorna i campi del record associato a quel oggetto tramite l'id
	*/
	public function update($validate = true) {
		if ($this->id) {
			if($validate && !$this->validate("update"))return false;
			$this->callbacks("beforeUpdate");
			$this->has_one_save();
			$table = static::tableName();
			$update = "UPDATE `$table` SET ";
			$this->eachFieldsSafe(function ($key, $value)use (&$update) {
				$update .= "`$key`=$value, ";
			});
			$update = substr($update, 0, -2).' ';
			$update .= static::where(array('id' => $this->id));
			$result = static::query($update);
			if($result){
				Cache::clearAll();
				$this->has_many_save($validate);
				$this->has_many_to_many($validate);
			}
			return $result;
		}
		return false;
	}
	private function has_one_save() {
		foreach ((array)static::$has_one as  $one_class) {
			if($this->{$one_class}){
				if(get_class($this->{$one_class})== ucfirst($one_class)){
					if($this->{$one_class} && $this->{$one_class}->id){
						$this->{"id_$one_class"} = $this->{$one_class}->id;
					}
				}
			}
		}

	}
	private function has_many_save($validate) {
		foreach ((array)static::$has_many as $many_class) {
			foreach ((array)$this->{$many_class} as $elem) {
				$elem->save($validate);
			}
		}
	}
	private function has_many_to_many($validate) {
		foreach ((array)static::$has_many_to_many as $key => $many_class) {
			if($this->{$key}){
				if(is_array($many_class)){
					$many_table = substr($many_class[1], 0, -1);
					$key_t = substr($many_class[0], 0, -1);
				}else{
					$many_table = substr($many_class, 0, -1);
					$key_t = substr($key, 0, -1);;
				}
				$table_t = strtolower(substr(static::tableName(), 0, -1));
				foreach ($this->{$key} as $value) {
					if(!$value->id) $value->save($validate);
					$many_table::find_and_create(array("id_$table_t"=>$this->id,"id_$key_t"=>$value->id));
				}
			}
		}
	}
	/*
	 * Sanitizza i dati e chiama una callback con i dati sicuri, però questo avviene solo nel caso i dati
	* fanno parte dei campi della tablella
	*/
	private function eachFieldsSafe($callback) {
		$fields = static::describeTable();

		foreach ($fields as $field => $property) {
			$key = static::safe($field);
			if($this->data[$field] == null && $property['Null'] == 'YES'){
				if($property['Default'])
				$value = "'".$property['Default']."'";
				else $value = 'NULL';
			}else {
				$value = "'".static::safe($this->{$field})."'";
			}
			$callback($key,$value);
		}
	}

	/*
	 * Sanitizza sia la key che il value di un hash dato in input
	*/
	static private function eachSafe($data,$callback) {
		foreach ((array)$data as $key => $value) {
			$key = static::safe($key);
			$value = static::safe($value);
			$callback($key,$value);
		}
	}
	private function each($data, $callback) {
		foreach ((array)$data as $key => $value) {
			$callback($key,$value);
		}
	}
	/*
	 * Salva l'oggetto nel database con una INSERT
	*/
	public function save($validate = true) {
		if($validate && !$this->validate("create"))return false;
		$this->callbacks("beforeSave");
		$table = static::tableName();
		Cache::clearAll();
		$insert = "INSERT INTO `$table` (";
		$values = "VALUES (";
		if (!$this->id) {
			$this->id = $this->lastId()+1;
		}else{
			$a = static::find_by_id($this->id);
			if(!empty($a))
			return false;
		}
		$this->has_one_save();

		$fields = static::describeTable();
		if(array_key_exists("created_at", $fields)){
			$this->created_at = date("Y-m-d H-i-s");
		}
		$this->eachFieldsSafe(function ($key, $value)use (&$insert, &$values) {

			$insert .= "`" . $key . "`, ";
			$values .= "$value, ";
		});
		$insert = substr($insert, 0, -2) . ")";
		$values = substr($values, 0, -2) . ")";
		$result = static::query($insert . ' ' . $values);
		if($result){
			$this->has_many_save($validate);
			$this->has_many_to_many($validate);
		}
		$this->callbacks("afterSave",$result);
		return $result;
	}
	/*
	 * Cancellare un record tabella che si riferisce all'oggetto
	*/
	public function delete($cond = null) {
		if ($this->id) {
			$this->callbacks("beforeDelete");
			foreach ((array)static::$has_many_to_many as $many_class) {
				if(is_array($many_class)){
					$many_table =substr($many_class[1], 0, -1);
				}else{
					$many_table =substr($many_class, 0, -1);
				}
				$table = substr(static::tableName(), 0, -1);
				$all_ris = $many_table::find(array("id_$table"=>$this->id));
				foreach ($all_ris as $ris) {
					$ris->delete();
				}
			}
			$table = static::tableName();
			$delete = "DELETE FROM `$table` ";
			$cond =(array)$cond;
			$cond += array('id' => $this->id);
			$delete .= static::where($cond);
			static::query($delete);
			$numRows = mysql_affected_rows();
			if($numRows){
				Cache::clearAll();
			}
			$this->callbacks("afterDelete",$numRows);
			return $numRows;
		}
		return 0;
	}
	static public function deleteAll() {
		$table = static::tableName();
		$delete = "DELETE FROM `$table` WHERE 1";
		static::query($delete);
		$numRows = mysql_affected_rows();
		if($numRows){
			Cache::clearAll();
		}
		return $numRows;
	}
	/*
	 * Converte in un array associativo l'oggetto riferito
	*/
	public function getArray() {
		foreach ((array)$this->data as $key => $value) {
			$res[$key] = $value;
		}
		return $res;
	}
	static private function each_mysql_row($result,$callback) {
		$ris = array();
		static::$last_query_count = mysql_num_rows($result);
		if ($result) {
			while ($row = mysql_fetch_assoc($result)) {
				$ris [] = $callback($row);
			}
			mysql_free_result($result);
		}
		return $ris;
	}
	/*
	 * Restituisce un array di oggetti derivanti da query
	*/
	static private function getResult($result) {
		$class = get_called_class();
		return static::each_mysql_row($result,function ($row) use ($class) {
			return  new $class($row);
		});
	}
	static private function getOnlyId($result,$field) {
		$final_field = 'id';
		if(is_string($field)){
			$fields = static::describeTable();
			if(array_key_exists($field, $fields)){
				$final_field = $field;
			}
		}
		return static::each_mysql_row($result,function ($row) use ($final_field) {
			return  $row[$final_field];
		});
	}
	/*
	 * Crea una Query SELECT completa in base a delle condizioni
	* $cond	hash con le codizioni del where
	* $optino 	hash order, limit, first(Restituisce un oggetto, non un array)
	*/
	static private function buildQuery($cond, $option = null) {
		if(is_int($cond)){
			$cond = array("id"=>$cond);
		}
		$option =  (array)$option + array("order"=>null,"limit"=>null,"first"=>null,"where"=>null,"onlyIds"=>null,"select"=>null);
		$query = static::select($option["select"]);
		$query .= static::where($cond);
		if($cond){
			$query .= static::where_unsafe($option["where"]);
		}else{
			if($option["where"]){
				$query .= "WHERE ";
				$query .= static::where_unsafe($option["where"]);
			}
		}
		$query .= static::order($option["order"]);
		if(!$option["limit"] &&  $option["first"]){
			$option["limit"] = '1';
		}
		$query .= static::limit($option["limit"]);
		return $query;
	}
	static public function find($cond, $option= null) {
		$option =  (array)$option + array("order"=>null,"limit"=>null,"first"=>null,"where"=>null,"onlyIds"=>null,"select"=>null);
		$query = static::buildQuery($cond,$option);

		if($option["onlyIds"]){
			try {
				$ris = Cache::get($query+'id');
			} catch (Exception $e) {
				$result = static::query($query);
				$ris = static::getOnlyId($result,$option["onlyIds"]);
				Cache::add($query+'id', $ris);
			}
		}
		else{
			try {
				$ris = Cache::get($query);
			} catch (Exception $e) {
				$result = static::query($query);
				$ris = static::getResult($result);
				Cache::add($query, $ris);
			}
		}
		if((isset($cond["id"])||  $option["first"])){
			if(isset($ris[0])){
				$ris=$ris[0];
			}else{
				$ris=null;
			}
		}
		return $ris;
	}
	static public function find_by_sql($query,$param=null) {
		if($param){
			$num = count($param);
			$dollar= array();
			for ($i = 1; $i <= $num; $i++) {
				$dollar []= '$'.$i;
			}
			$query = str_replace($dollar,$param,$query);
		}
		try {
			$result = Cache::get($query);
		} catch (Exception $e) {
			$result = static::query($query);
			$result = static::getResult($result);
			Cache::add($query, $result);
		}
		return $result;
	}
	/*
	 * Versione base espanderlo
	*/
	static private function order($param= null) {
		$order ="";
		if($param){
			$table = static::tableName();
			$order = "ORDER BY ";
			if(is_array($param)){
				static::eachSafe($param,function ($key, $value)use (&$order) {
					$order .= "$value, ";
				});
				$order = substr($order, 0, -2)." ";
			}else
			$order .= static::safe($param);

		}
		return $order.' ';

	}
	/*
	 * Versione base espanderlo
	*/
	static private function limit($param=null) {
		$limit ="";
		if($param){
			$param = static::safe($param);
			$limit = "LIMIT $param ";
		}
		return $limit.' ';
	}
	/*
	 * Restituisce una stringa WHERE di una query, le condizioni sono sanitizzate
	*/
	static private function where($cond) {
		$where = "";
		if ($cond) {
			$where .= "WHERE";
			$table = static::tableName();
			$op = '=';
			Record::eachSafe($cond,function ($key, $value)use (&$where,$table,$op) {
				$simbol = substr($value, 0, 2); // >= <= < > <>
				if($simbol[0] == '>' || $simbol[0] == '<'){
					$op = $simbol[0];
					$value = substr($value,1);
					if($simbol[1] == '=' || $simbol[1] == '>'){
						$op .= $simbol[1];
						$value = substr($value, 1);
					}
				}
				$where .= " `$table`.`$key`$op'$value' AND";
			});
			$where = substr($where, 0, -3);
		}
		return $where.' ';
	}
	static private function where_unsafe($cond) {
		if($cond){
			return ' '.$cond.' ';
		}
	}
	/*
	 * Restituisce una query select di default con tutti i campi altrimenti con campi messi in un array di stringhe
	*/
	static private function select($fields = null) {
		$select = 'SELECT ';
		if($fields){
			foreach ((array)$fields as $value) {
				$select .= "$value, ";
			}
			$select = substr($select,0,-2);
		}else{
			$select .= '*';
		}
		$table = static::tableName();
		return "$select FROM `$table` ";
	}
	/*
	 *
	*/
	private function validate($action) {
		$class = get_called_class();
		$ris = true;
		$validation = new Validate($class,$this);
		foreach ((array)static::$validate as $value) {
			try{
				$field = $value["field"];
				unset($value["field"]);
				if(isset($value["on"])){
					$on =$value["on"];
					unset($value["on"]);
					if($on == $action){
						$validation->validateValue($this->$field, $field, $value);
					}
				}else {
					$validation->validateValue($this->$field, $field, $value);
				}
			}catch (Exception $exc){
				$this->errors []= $exc->getMessage();
				$ris = false;
			}
		}
		return $ris;
	}
	public function serialize() {
		return serialize($this->data);
	}
	public function unserialize($data) {
		$this->data = unserialize($data);
	}
}

?>
