<?php
require 'Record.php';
require 'Validate.php';
$config["mysqlHost"]="localhost";
$config["mysqlUser"]="root";
$config["mysqlPsw"]="root";
$config["mysqlDb"]="testDB";
class TestTable extends Record{
	static function genRandom() {
		new TestTable();
	}
}
class User extends Record{
	public static $has_many = array("preferences");
	public static $has_many_to_many =  array("prefSeries"=>array("tvseries","preferences"));

}
class Preference extends Record{
	public static $has_one= array("user","tvseries");
}
class Tvseries extends Record{
	public static $has_many_to_many =  array("prefUsers"=>array("users","preferences"));
	public static $has_many = array("preferences");
}
function myAssert($a, $b,$mess) {
	if(!($a == $b)) {
		if($mess)echo "$mess<br>";
		var_dump($a);
		var_dump($b);
	}
}
function myOneAssert($a, $mess = null) {
	if(!$a) {
		if($mess)echo "$mess<br>";
		var_dump($a);
	}
}
function saveAndDeleteAndUpdateTest() {
	echo "SaveTest<br>";
	$obj = new TestTable(array("id"=>32,"text"=>"ciao","intero"=>1));
	myOneAssert($obj->save(),"Primo test deve salvare un indice gia' passato");
	myOneAssert(!$obj->save(),"Non deve salvare perchè l'elemento esiste gia'");
	$obj->text = "nuovo testo";
	myOneAssert($obj->update(),"Deve aggiornare l'oggetto");
	$obj1 = TestTable::find_first_by_id($obj->id);
	myAssert($obj1->text,$obj->text, "I testi sono diversi non è stato aggiornato il record");
	$obj = new TestTable(array("text"=>"ciao","intero"=>1));
	myOneAssert($obj->save(),"1.Deve salvare perchè assegna dinamicanmente l'indice");
	myOneAssert(!$obj->save(),"Non deve salvare perchè il salvataggio precedenti gli è stato assegnato un indice");
	myOneAssert($obj->delete(),"Deve cancellare l'elemento che esite");
	myOneAssert(!$obj->delete(),"Non deve fare niente perchè non esiste l'elemento");
	TestTable::deleteAll();
}
function OrderLimitWhere() {
	echo "Order Limit Test<br>";
	$array = array();
	$array []= new TestTable(array("text"=>"ciao","intero"=>1));
	$array []=  new TestTable(array("text"=>"prova","intero"=>2));
	$array []=   new TestTable(array("text"=>"prova","intero"=>3));
	$array []=   new TestTable(array("text"=>"ciao","intero"=>4));
	$array []=   new TestTable(array("text"=>"come","intero"=>5));
	$array []=   new TestTable(array("text"=>"va","intero"=>6));
	foreach ($array as $value) {
		$value->save();
	}
	echo "<>ciao<br>";
	var_dump(TestTable::find(array("text"=>"<>ciao")));
	echo "<=5<br>";
	var_dump(TestTable::find(array("intero"=>"<=5")));
	echo ">5<br>";
	var_dump(TestTable::find(array("intero"=>">5")));
	echo ">=3<br>";
	var_dump(TestTable::find(array("intero"=>">=3")));
	TestTable::deleteAll();
}
function reletion_test() {
	echo "Reletions TEST <br><br>";
	User::deleteAll();
	Tvseries::deleteAll();
	Preference::deleteAll();
	$u1 = new User(array("username"=>"prova1"));
	$u1->save();
	$u2 = new User(array("username"=>"prova21"));
	$u2->save();
	$ts1 = new Tvseries(array("title"=>"Bones"));
	$ts1->save();
	$ts2 = new Tvseries(array("title"=>"CSI"));
	$ts2->save();
	$ts3 = new Tvseries(array("title"=>"Heros"));
	$ts3->save();
	$p1 = new Preference(array("id_users"=> $u1->id,"id_tvseries"=>$ts1->id,"newsletter"=>"1"));
	$p1->save();
	$p2 = new Preference(array("id_users"=> $u1->id,"id_tvseries"=>$ts2->id,"newsletter"=>"0"));
	$p2->save();
	$p2 = new Preference(array("id_users"=> $u2->id,"id_tvseries"=>$ts3->id,"newsletter"=>"1"));
	$p2->save();
	$p2 = new Preference(array("id_users"=> $u1->id,"id_tvseries"=>$ts3->id,"newsletter"=>"1"));
	$p2->save();

	var_dump( $u1->prefSeries);
	var_dump($u1);
	var_dump( $ts3->prefUsers);
	var_dump($ts3);
	/*	 foreach ($psu1 as $pu1)
	 var_dump($pu1->tvseries);
	var_dump($u1);
	/* $psts2 =  $ts1->preferences;
	var_dump($psts2);
	foreach ($psts2 as $pu1) {
	var_dump($pu1);
	}*/

}
Record::connect($config["mysqlHost"],$config["mysqlUser"],$config["mysqlPsw"],$config["mysqlDb"]);

saveAndDeleteAndUpdateTest();
OrderLimitWhere();
reletion_test();
Record::close_connect();
?>