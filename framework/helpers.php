<?php

function addAttributes($attributes) {
	$string ="";
	foreach ((array)$attributes as $key => $value) {
		$string .= $key.'="'.safe($value).'" ';
	}
	return $string;
}
function tagClose($tag,$text = "", $attributes=null,$htmlentities =false) {
	$string = "<$tag ";
	$string .= addAttributes($attributes);
	return $string .= ">$text</$tag>";
}
function safe($text) {
	if($text)
	return htmlentities($text, ENT_COMPAT, "UTF-8");
	else
	return '';
}
function tagNotClose($tag,$attributes) {
	$string = "<$tag ";
	$string .= addAttributes($attributes);
	return $string .= "/>";
}
function img($src, $attributes=array()) {
	$path =host("public/images/$src");
	$attributes += array("src"=>$path);
	return tagNotClose("img",$attributes);
}
function script($src, $attributes=array()) {
	$path =host("public/scripts/$src");
	$attributes += array("type"=>"text/javascript","src"=>$path);
	return tagClose("script","",$attributes);
}

function css($src, $attributes=array()) {
	$path =host("public/css/$src");
	$attributes += array("rel"=>"stylesheet", "type"=>"text/css","href"=>$path);
	return tagNotClose("link",$attributes);
}
function formSubmit($text,$attributes=array()) {
	return formButton($text,array("type"=>"submit")+$attributes);
	 
}
function formButton($text,$attributes=array()) {
	return tagClose("button",$text,$attributes);
}
function formReset($attributes=array()) {
	return formInput("reset",$attributes);
}
function formText($attributes=array()) {
	return formInput("text",$attributes);
}
function formInput($type, $attributes=array()) {
	$attributes += array("type"=>$type);
	return tagNotClose("input",$attributes);
}
function host($relPath="") {
	$path = Routing::$beginningUri;
	if($relPath[0] == '/') $relPath= substr($relPath, 1);
	return "http://$path".$relPath;
}

function uri($getArray) {
	$add='';
	if(is_array($getArray)){
		if(isset($getArray["add"])){
			$add = $getArray["add"];
			unset($getArray["add"]);
		}
		$link = array();
		foreach ($getArray as $value) {
			$link []= urlencode($value);
		}
	}else{
		if($getArray == "index"){
			$link ="";
		}else $link = $getArray;
	}
	$link = Routing::getCompleteUri($link);
	return $link.$add;
}

function uridecode($uri) {
	return urldecode(str_replace("_", " ", $uri));
}

function uriencode($uri) {
	return urlencode(strtolower(str_replace("/", "_", str_replace(" ", "_", $uri))));
}

function link_to($uri, $string, $attributes=array()) {
	$path = uri($uri);
	$attributes = (array)$attributes;
	$this_uri = 'http://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
	if($this_uri == $path){
		if(isset($attributes['class']))$attributes['class'].=' selected';
		else
		$attributes  += array("class "=>'selected');
	}
	$attributes += array("href"=>$path);
	return tagClose("a",$string,$attributes);
}
//Date
function strDate($date, $exit_format='d/F/Y', $in_format='Y-m-d') {
	$string_date= '';
	if($date){
		$date = date_create_from_format($in_format, $date);
		if($date){
			$string_date = $date->format($exit_format);
		}
	}
	return $string_date;
}
function strDateTime($datetime) {
	if($datetime)
	return date("d/m/y G:i:s", strtotime($datetime));
}
function array_ids($array_obj,$field = 'id') {
	$array_ids = array();
	foreach ((array)$array_obj as $value) {
		$array_ids []= $value->$field;
	}
	return $array_ids;
}
//Array File
function multiple(array $_files, $top = TRUE)
{
	$files = array();
	foreach($_files as $name=>$file){
		if($top) $sub_name = $file['name'];
		else    $sub_name = $name;

		if(is_array($sub_name)){
			foreach(array_keys($sub_name) as $key){
				$files[$name][$key] = array(
                    'name'     => $file['name'][$key],
                    'type'     => $file['type'][$key],
                    'tmp_name' => $file['tmp_name'][$key],
                    'error'    => $file['error'][$key],
                    'size'     => $file['size'][$key],
				);
				$files[$name] = multiple($files[$name], FALSE);
			}
		}else{
			$files[$name] = $file;
		}
	}
	return $files;
}

// this is the function that will create the thumbnail image from the uploaded image
// the resize will be done considering the width and height defined, but without deforming the image
function make_thumb($img_name,$filename,$new_w,$new_h,$max_w,$max_h)
{
	//get image extension.
	$ext=getExtension($img_name);
	//creates the new image using the appropriate function from gd library
	if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext))
	$src_img=imagecreatefromjpeg($img_name);

	if(!strcmp("png",$ext))
	$src_img=imagecreatefrompng($img_name);

	//gets the dimmensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// next we will calculate the new dimmensions for the thumbnail image
	// the next steps will be taken:
	// 1. calculate the ratio by dividing the old dimmensions with the new ones
	// 2. if the ratio for the width is higher, the width will remain the one define in WIDTH variable
	// and the height will be calculated so the image ratio will not change
	// 3. otherwise we will use the height ratio for the image
	// as a result, only one of the dimmensions will be from the fixed ones
	$rapporto = $old_x/$old_y;
	if($new_w == 0){
		$new_w =  $new_h*$rapporto;
		if($max_w && $old_w>$max_w){
			$new_w = $max_w;
		}
	}
	if($new_h == 0){
		$new_h =   $new_w/$rapporto;
		if($max_h && $old_y>$max_h){
			$new_h = $max_h;
		}
	}
	$ratio1=$old_x/$new_w;
	$ratio2=$old_y/$new_h;
	if($ratio1>$ratio2) {
		$thumb_w=$new_w;
		$thumb_h=$old_y/$ratio1;
	}
	else {
		$thumb_h=$new_h;
		$thumb_w=$old_x/$ratio2;
	}

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	if(!strcmp("png",$ext))
	imagepng($dst_img,$filename);
	else
	imagejpeg($dst_img,$filename);

	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}

// This function reads the extension of the file.
// It is used to determine if the file is an image by checking the extension.
function getExtension($str) {
	$i = strrpos($str,".");
	if (!$i) {
		return "";
	}
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}
function createthumb($name,$filename,$new_w,$new_h)
{
	$system=explode(".",$name);

	$src_img=imagecreatefromjpeg($name);

	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	$rapporto = $old_x/$old_y;
	if($new_w == 0){
		$thumb_w =  $new_h*$rapporto;;
		$thumb_h =  $new_h;
	}
	if($new_h == 0){
		$thumb_w = $new_w;
		$thumb_h =   $new_w/$rapporto;
	}
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

	imagejpeg($dst_img,$filename);

	imagedestroy($dst_img);
	imagedestroy($src_img);
}
?>
