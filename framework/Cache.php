<?php

class Cache{
	private static $data = array();
	private static $change = false;
	private static $filename = 'public/cache.dat';
	public static function init() {
		if(file_exists(static::$filename)){
			$string = file_get_contents(static::$filename);
			if($string){
				static::$data = unserialize($string);
			}
		}
	}
	public static function end() {
		if(static::$change){
			$string = serialize(static::$data);
			file_put_contents(static::$filename, $string);
		}
	}
	private static function prep_key($key) {
		return md5($key);
	}
	public static function exists($key) {
		$key = static::prep_key($key);
		return isset(static::$data[$key]);
	}
	public static function get($key) {
		//if(static::exists($key)){
		//	$key = static::prep_key($key);
		//	return static::$data[$key];
		//}else{
			throw new Exception("Chiave non trovata",1);
		//}
	}
	public static function add($key, $value) {
		$key = static::prep_key($key);
		static::$data[$key]=$value;
		static::$change=true;
		return true;
	}
	public static function delete($key) {
		if(static::exists($key)){
			$key = static::prep_key($key);
			unset(static::$data[$key]);
			static::$change=true;
		}
		return true;
	}
	public static function clearAll(){
		static::$change=true;
		static::$data=array();
		return true;
	}
}
