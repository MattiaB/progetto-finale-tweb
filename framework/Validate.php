<?php
class Validate{
	private $value;
	private $class;
	private $obj;
	private $field;
	function __construct($class, $obj) {
		$this->class =$class;
		$this->obj = $obj;
	}
	function validateValue($value, $field, $validate) {
		$this->field = $field;
		$this->value = $value;
		$validate['rule']= (array)$validate['rule'];
		$func = $validate['rule'][0];
		unset($validate['rule'][0]);
		if(!call_user_func_array("Validate::$func",$validate['rule']))
		throw new Exception($validate['message']);

	}
	function email(){
		if ($this->value) {
			if (preg_match('/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/', $this->value))
			return true;
		}
		return false;
	}
	function comparison($comp,$val) {

	}
	function isUnique() {
		$class = $this->class;
		$a = $class::find(array($this->field=>$this->value));
		return empty($a);
	}
	function minLength($min) {
		if(strlen($this->value) >= intval($min))
		return true;
		return false;
	}
	function maxLength($min) {
		if(strlen($this->value) <= intval($min))
		return true;
		return false;
	}
	function inList( $array) {
		return in_array($this->value, $array);
	}
	function notEmpty() {
		return !(trim($this->value) ==  '');
	}
	function range($min, $max) {
		if(intval($this->value) >= intval($min)){
			if(intval($this->value) <= intval($max)){
				return true;
			}
		}
		return false;
	}
	function date() {
		$ris = false;
		if($this->value){
			foreach ((array)func_get_args() as $value) {
				$p = date_create_from_format($value,$this->value);
				if($p){
					$ris |= true;
				}
			}
		}else $ris |= true;
		return $ris;
	}
	function alphaNumeric() {
		return ctype_alnum($this->value);
	}
	function numeric() {
		if($this->value){
			return is_numeric($this->value);
		}else return true;
	}
	function greater_of_date($field) {
		if($this->value && $this->obj->$field){
			$greater =  strtotime($this->value);
			$less = strtotime($this->obj->$field);
			if($greater > $less)	return true;
			else return false;
		}
		return true;
	}
}

?>