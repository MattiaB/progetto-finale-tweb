<?php
$route["(\d{4})\/(\d{2})\/(\d{2})\/?(.*)?"] = "blog/index/$1/$2/$3/$4";

$route["(signup)"] 							= "users/create";
$route["(login)"] 							= "sessions";
$route["(login\/create)"] 					= "sessions";
$route["(logout)"] 							= "sessions/destroy";
$route["(.*?)\/(.*?)?\/(.*)?"] 				= "$1/$2/$3";

$inverseRoute["users\/create"] 				= "signup";
$inverseRoute["sessions\/create"] 			= "login/create";
$inverseRoute["sessions"] 					= "login";
$inverseRoute["sessions\/destroy"] 			= "logout";

?>
