<?php
class Episode extends ApplicationRecord{
	public static $has_one = array('series','user','season');
	public static $validate= array(
	array(
						'field'=>'title',
						'rule'=> array('minLength',2),
						'message'=> "Il titolo è troppo corto deve essere più lungo di due caratteri"),
	array(
						'field'=>'rate',
						'rule'=> array("range",0,5),
						'message'=> "Il voto deve essere compreso tra 1 e 5"),
	array(
						'field'=>'original_air_date',
						'rule'=> array("date",'d/m/Y','Y-m-d'),
						'message'=> "Inserire un formato di data valido nel campo data di trasmissione es. 10/12/2010"),
	array(
						'field'=>'no_in_series',
						'rule'=> 'notEmpty',
						'message'=> "Il numero dell'episodio deve essere presente"),
	array(
						'field'=>'no_in_series',
						'rule'=> 'numeric',
						'message'=> "Il numero dell'episodio nella serie deve essere un numero"),
	array(
						'field'=>'no_in_season',
						'rule'=> 'numeric',
						'message'=> "Il numero dell'episodio nella stagione deve essere un numero")
	);
	function beforeSave() {
		$this->format_date("original_air_date");
		$this->created_at = date("Y-m-d H-i-s");
	}
	function afterSetData() {
		$this->format_date("original_air_date");
	}
	function beforeUpdate() {
		$this->format_date("original_air_date");
	}

}