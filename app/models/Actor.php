<?php
class Actor extends ApplicationRecord{
	public static $has_many_to_many = array('seriess'=>'actorsSeriess');
	public static $validate= array(
	array(
						'field'=>'name',
						'rule'=> array('minLength',2),
						'message'=> "Il dell'attore è troppo corto"),
	array(
						'field'=>'name',
						'rule'=> 'isUnique',
						'message'=> "L'attore esiste già")
	);
	static function find_or_save($name) {
		$actor = Actor::find_first_by_name($name);
		if(!$actor){
			$actor = new Actor(array("name"=>$name));
			$actor->save();//controllare se il salvataggio è avvenuto correnttamente
		}
		return $actor;
	}
	function __toString() {
		return $this->name;
	}
}