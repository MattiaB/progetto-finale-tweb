<?php
class Wiki {
	private static $xpath;
	private static $host;
	private static $link;
	public static $series_array_default =array();
	public static $season_array_default=array();
	public static $episode_array_default=array();
	static function init($link) {
		if($link != Wiki::$link){
			$opts = array(
			'http'=>array(
				'method'=>"GET",
		        'header'=>"Accept-language: en\r\n" .
				"User-Agent: Mozilla/5.0 (X11; U; Linux x86_64; es-AR; rv:1.9.2.23) Gecko/20110921 Ubuntu/10.10 (maverick) Firefox/3.6.23"
			)
			);
			$context = stream_context_create($opts);
			$html = file_get_contents($link, false, $context);
			$doc = new DOMDocument();
			@$doc->loadHTML($html);
			Wiki::$xpath = new DOMXpath($doc);
			preg_match("/http:\/\/.*?\//", $link,$matches);
			if(!isset($matches[0])) throw new Exception("Non è possibile parsificare l'host");
			Wiki::$host =substr($matches[0], 0,-1);
			Wiki::$link = $link;
		}
	}
	static function get_value_first_element($query) {
		$dom_elements= Wiki::$xpath->query($query);
		if($dom_elements){
			if($dom_elements->length >0){
				$elem =$dom_elements->item(0);
				return $elem->nodeValue;
			}
		}
		return null;
	}
	static function new_series() {
		return new Series(static::$series_array_default);
	}
	static function new_season() {
		return new Season(static::$season_array_default);
	}
	static function new_episode() {
		return new Episode(static::$episode_array_default);
	}
	static function new_parse_series($link,$user) {
		Wiki::init($link);
		if(Wiki::$xpath->query("//table[@class='infobox vevent']")->length == 0) return null;
		$series = static::new_series();
		$series->user= $user;
		$series->title= trim(Wiki::get_value_first_element("//table[@class='infobox vevent']//tr[position()=1]/th"));
		$series->description =trim(Wiki::get_value_first_element("//div[@class='mw-content-ltr']/p[position()=1]"));
		$series->description = preg_replace("/\[\d*\]/", "", $series->description);
		$dom_genres = Wiki::$xpath->query("//table[@class='infobox vevent']//tr[th='Genre']/td");
		$genres=array();
		if($dom_genres->length){
			foreach ($dom_genres->item(0)->childNodes as $dom_genre) {
				if(trim($dom_genre->nodeValue)!='')
				$genres []= Genre::find_and_create(array("name"=>trim($dom_genre->nodeValue)));
			}
		}

		$series->genres = $genres;
		$dom_starring = Wiki::$xpath->query("//table[@class='infobox vevent']//tr[th='Starring']/td//ul");
		if(!$dom_starring->length){
			$dom_starring = Wiki::$xpath->query("//table[@class='infobox vevent']//tr[th='Starring']/td");
		}

		if($dom_starring->length){
			foreach ($dom_starring->item(0)->childNodes as $dom_star) {
				if(!preg_match("/\[\d*\]/",$dom_star->nodeValue) && trim($dom_star->nodeValue)!='' && $dom_star->nodeName != 'small'){
					$value = trim(preg_replace("/\[\d*\]/", "", $dom_star->nodeValue));
					$actors []=  Actor::find_and_create(array("name"=>$value));
				}
			}
		}
		$series->actors = $actors;
		$series->no_of_seasons = trim(Wiki::get_value_first_element("//table[@class='infobox vevent']//tr[th='No. of seasons']/td"));
		$dom_channel = trim(Wiki::get_value_first_element("//table[@class='infobox vevent']//tr[th='Original channel']/td"));
		if(!preg_match("/\(.*\)/", $dom_channel)){
			$series->channel = Channel::find_and_create(array("name"=>$dom_channel));
		}

		$dom_no_of_episodes = trim(Wiki::get_value_first_element("//table[@class='infobox vevent']//tr[th='No. of episodes']/td"));
		if(	preg_match("/\d*/", $dom_no_of_episodes,$matches)){
			$series->no_of_episodes = $matches[0];
		}

		$series->date_start = trim(Wiki::get_value_first_element("//table[@class='infobox vevent']//tr[th='Original run']/td/span[position()=1]/span"));
		$dom_date_end = Wiki::$xpath->query("//table[@class='infobox vevent']//tr[th='Original run']/td/span[position()=2]/span");
		if($dom_date_end->length){
			$series->date_end=trim($dom_date_end->item(0)->nodeValue);
		}
		$date = trim(Wiki::get_value_first_element("//table[@class='infobox vevent']//tr[th='Original run']/td"));
		$date = iconv("UTF-8", "ASCII//TRANSLIT", $date);
		preg_match_all('/\w{5,9}\s\d{1,2}[,?]\s\d{4}|\d{1,2}\s\w{5,9}\s\d{4}/',$date,$matches);
		if(isset($matches[0]))
		$matches = $matches[0];
		if(isset($matches[0]))
		$series->date_start = date('Y-m-d',strtotime($matches[0]));
		if(isset($matches[1])){
			$series->date_end = date('Y-m-d',strtotime($matches[1]));
		}

		$list_episodes_link = Wiki::$xpath->query("//table[@class='infobox vevent']//tr[th='No. of episodes']/td//a");
		if($list_episodes_link->length){
			$link = Wiki::$host.$list_episodes_link->item($list_episodes_link->length-1)->getAttribute("href");
			$series->seasons = Wiki::new_parse_season($link, $series,$user);
			$series->episodes = Wiki::new_parse_episodes($link,$series,$user);
		}
		return $series;
	}
	static function new_parse_season($link, $series,$user) {
		Wiki::init($link);
		$seasons=array();
		$table_seasons = Wiki::$xpath->query("//div/h2[span='Series overview']/../table[@class='wikitable'][1]//tr[th='Episodes']/../tr[position()>2]");
		if($table_seasons->length){
			foreach ($table_seasons as $table_season) {
				$season =static::new_season();
				$y=0;
				foreach ( $table_season->childNodes as $field) {
					if(trim($field->nodeValue)!=''){
						if($y == 0){
							if(intval($field->nodeValue))
							$season->number_season = $field->nodeValue;
							else{
								$season=null;
								break;
							}
						}
						if($y == 1){
							if(intval($field->nodeValue))
							$season->number_episodes = $field->nodeValue;
							else $y--;
						}
						$y++;
					}
				}
				if($season){
					$season->series=$series;
					$season->user=$user;
					$seasons [] = $season;
				}
			}
		}
		return $seasons;
	}
	static function new_parse_episodes($link, $series,$user) {
		Wiki::init($link);
		$episodes =array();
		$link_episodes_seasons = Wiki::$xpath->query("//div[@class='rellink' or @class='rellink relarticle mainarticle']/a");
		if($link_episodes_seasons->length==0){
			$episodes = Wiki::new_parse_episodes_of_more_seasons($series,$user);
		}else{
			foreach ($link_episodes_seasons as $link_season) {
				$link = Wiki::$host.$link_season->getAttribute("href");
				Wiki::init($link);
				preg_match("/\(.*?(\d*)\)/", $link_season->nodeValue,$matches);
				$season=null;
				foreach ($series->seasons as $season) {
					if($season->number_season==$matches[1])break;
				}
				if($season) $episodes = array_merge($episodes,Wiki::new_parse_episodes_season($season,$series,$user));
			}
		}
		return $episodes;
	}
	static function new_parse_episodes_of_more_seasons($series,$user) {
		$dom_episodes = Wiki::$xpath->query("//table[@class='wikitable']//tr[@class='vevent']");
		$seasons = $series->seasons;
		$episodes=array();
		if($season = current($seasons)){
			if($dom_episodes->length){
				foreach ($dom_episodes as $dom_episode) {
					$obj_episode = Wiki::new_parse_one_episodes($dom_episode, $season, $series,$user);
					$episodes []=$obj_episode;
					if($obj_episode->no_in_season == $season->number_episodes) $season = next($seasons);
				}
			}
		}
		return $episodes;
	}
	static function new_parse_episodes_season($season,$series,$user) {
		$dom_episodes = Wiki::$xpath->query("//table[@class='wikitable']//tr[@class='vevent']");
		$episodes=array();
		if($dom_episodes->length){
			foreach ($dom_episodes as $dom_episode) {
				$obj_episode = Wiki::new_parse_one_episodes($dom_episode, $season, $series,$user);
				$episodes []=$obj_episode;
			}
		}
		return $episodes;
	}
	static function new_parse_one_episodes($dom_episode,$season,$series,$user) {
		$obj_episode = static::new_episode();
		$obj_episode->season = $season;
		$obj_episode->series = $series;
		$obj_episode->user = $user;
		$field_i = 0;
		foreach ($dom_episode->childNodes as $node1) {
			if(trim($node1->nodeValue)!=''){
				if($field_i == 0){
					$obj_episode->no_in_series = trim($node1->nodeValue);
				}
				if($field_i == 1){
					if(intval($node1->nodeValue)){
						$obj_episode->no_in_season = trim($node1->nodeValue);
					}else{
						$obj_episode->no_in_season = $obj_episode->no_in_series;
						$field_i=2;
					}
				}
				if($field_i == 2){
					preg_match('/"(.*)"/',$node1->nodeValue,$matches);
					if(isset($matches[1]))
					$obj_episode->title = $matches[1];
				}
				if(preg_match('/\d{4}\-\d{2}\-\d{2}/',$node1->nodeValue,$matches)){
					$obj_episode->original_air_date =$matches[0];
				}
				$field_i++;
			}
		}
		$obj_episode->description = substr(trim($dom_episode->nextSibling->firstChild->nodeValue),0,-2);
		$obj_episode->description = preg_replace("/\[\d*\]/", "", $obj_episode->description);
		$obj_episode->series = $series;
		$obj_episode->season = $season;
		return $obj_episode;
	}

}
/*

static function parse_series($link) {
Wiki::init($link);
$elements = Wiki::$xpath->query("//table[@class='infobox vevent']");
$series = new Series();
$series->description = "";
$genres = array();
$actors = array();
if (!is_null($elements)) {
$element = $elements->item(0);
$nodes = $element->childNodes;
$i_title=0;
$a_date = array("date_start","date_end");
$i_date = 0;
foreach ($nodes as $node) {
$i = '';
foreach ($node->childNodes as $node1) {
if($node1->nodeName== 'th'){
if($node1->nodeValue == 'Genre' || $node1->nodeValue == 'Starring' || $node1->nodeValue == 'No. of seasons' || $node1->nodeValue == 'Original channel' || $node1->nodeValue == 'Original run'){
$i = $node1->nodeValue;
}else{
$i = '';
}
if(!$i_title){
//echo 'Title: '.$node1->nodeValue.'<br>';
$series->title = $node1->nodeValue;
$i_title = 1;
}
}
if($node1->nodeName== 'td'){
foreach ($node1->childNodes as $node2) {
if($node2->nodeValue == '') continue;
if($i == 'Genre'){
//echo 'Genre: '.$node2->nodeValue.'<br>';
$genres []= Genre::find_and_create(array("name"=>$node2->nodeValue));
}
if($i == 'Starring'){
if(!preg_match("/\[\d\]/",$node2->nodeValue) && $node2->nodeName != 'small'){
//echo 'Starring: '.$node2->nodeValue.'<br>';
$actors []=  Actor::find_and_create(array("name"=>$node2->nodeValue));
}
}
if($i == 'No. of seasons'){
//echo 'No. of seasons: '.$node2->nodeValue.'<br>';
$series->no_of_seasons =  $node2->nodeValue;
}
if($i == 'Original channel'){
//echo 'Original channel: '.$node2->nodeValue.'<br>';
$series->channel = Channel::find_and_create(array("name"=>$node2->nodeValue));
}
if($i == 'Original run' && $node2->nodeName == 'span'){
preg_match('/\d{4}\-\d{2}\-\d{2}/',substr($node2->nodeValue,1),$matches);
//echo 'Original run: '.$matches[0].'<br>';
$series->{$a_date[$i_date]} = $matches[0];
$i_date++;
}
}
}
}
}
$series->genres = $genres;
$series->actors = $actors;
return $series;
}
}
static function parse_seasons($link) {
Wiki::init($link);
$elements = Wiki::$xpath->query("//table[@class='wikitable']");
$i = 0;
$seasons = array();
if (!is_null($elements)) {
$element = $elements->item(0);
$nodes = $element->childNodes;
foreach ($nodes as $node) {
if($i <2){
$i++; continue;
}
$y = 1;
$season = new season();
foreach ( $node->childNodes as $node1) {
$y++;
if($y == 4){
$season->number_season = $node1->nodeValue;
}
if($y == 6){
$season->number_episodes = $node1->nodeValue;
}
}
$seasons [] = $season;
}
}
return $seasons;

}
static function parse_all_episodes($elements,$series) {
$i=0;
$episodes = array();
if (!is_null($elements)) {
foreach ($elements as $element) {
if(!$i){
$i = 1; continue;
}
$z=-1;
$season = season::find(array("number_season"=>$i,"id_series"=>$series->id),array("first"=>true));
$episode = null;
foreach ($element->childNodes as $node) {
if($z == -1){
$z = 0; continue;
}
if($z % 2 == 0){
$field_i = 0;
foreach ($node->childNodes as $node1) {
if(trim($node1->nodeValue)!=''){
if($field_i == 0){
$episode = new Episode(array("id_season"=>$season->id,"id_series"=>$series->id,"id_user"=>1));
//echo "no in series: ".$node1->nodeValue.'<br>';
$episode->no_in_series = $node1->nodeValue;
}
if($field_i == 1){
//echo "no in season: ".$node1->nodeValue.'<br>';
$episode->no_in_season = $node1->nodeValue;
}
if($field_i == 2){
//echo "title: ".$node1->nodeValue.'<br>';
$episode->title = substr(substr($node1->nodeValue, 0, -1) ,1);
}
if($field_i == 5){
//echo "original_air_date: ".$node1->nodeValue.'<br>';
preg_match('/\d{4}\-\d{2}\-\d{2}/',$node1->nodeValue,$matches);
if(!empty($matches))
$episode->original_air_date =$matches[0];
}
$field_i++;
}
}
}
if($z % 2 == 1){
foreach ($node->childNodes as $node1) {
if(trim($node1->nodeValue)!=''){
//echo "description: ".substr($node1->nodeValue, 0, -2).'<br>';
$episode->description = substr($node1->nodeValue, 0, -2);
//var_dump($episode);
$episodes []=$episode;
}
}
}
$z++;
}
$i++;
}
}
}
static function parse_one_season_episodes($elements,$series,$season) {
$episodes = array();
if (!is_null($elements)) {
foreach ($elements as $element) {
$z=-1;
$episode = null;
foreach ($element->childNodes as $node) {
if($z == -1){
$z = 0; continue;
}
if($z % 2 == 0){
$field_i = 0;
foreach ($node->childNodes as $node1) {
if(trim($node1->nodeValue)!=''){
if($field_i == 0){
$episode = new Episode(array("id_season"=>$season->id,"id_series"=>$series->id));
//echo "no in series: ".$node1->nodeValue.'<br>';
$episode->no_in_series = $node1->nodeValue;
}
if($field_i == 1){
//echo "no in season: ".$node1->nodeValue.'<br>';
$episode->no_in_season = $node1->nodeValue;
}
if($field_i == 2){
//echo "title: ".$node1->nodeValue.'<br>';
$episode->title = substr(substr($node1->nodeValue, 0, -1) ,1);
}
if($field_i == 6){
//echo "original_air_date: ".$node1->nodeValue.'<br>';
preg_match('/\d{4}\-\d{2}\-\d{2}/',$node1->nodeValue,$matches);
if(!empty($matches))
$episode->original_air_date =$matches[0];
}
$field_i++;
}
}
}
if($z % 2 == 1){
foreach ($node->childNodes as $node1) {
if(trim($node1->nodeValue)!=''){
//echo "description: ".substr($node1->nodeValue, 0, -2).'<br>';
$episode->description = substr($node1->nodeValue, 0, -2);
$episodes []= $episode;
//$episode->save();
}
}
}
$z++;
}
}
}
return $episodes;
}
static function parse_episodes($link,$series_id,$season_id) {
Wiki::init($link);
$elements = Wiki::$xpath->query("//table[@class='wikitable']");
$series = Series::find_first_by_id($series_id);
if($season_id){
$season = season::find_first_by_id($season_id);
return Wiki::parse_one_season_episodes($elements,$series,$season);
}else{
return Wiki::parse_all_episodes($elements,$series);
}
}
static function parse_more_season_episodes($link,$series,$episodes=array()) {
Wiki::init($link);
preg_match("/\(.*?(\d)\)/", $link,$matches1);
$season = season::find(array("id_series"=>$series->id,"number_season"=>$matches1[1]),array("first"=>true));
$episodes = array_merge($episodes,Wiki::parse_one_season_episodes(Wiki::$xpath->query("//table[@class='wikitable']"),$series,$season));
preg_match("/http:\/\/.*?\//", $link,$matches);
$host =substr($matches[0], 0,-1);
$elements = Wiki::$xpath->query("//table[@class='infobox vevent']//table//td[2]//a");
if($elements->length==0) return $episodes;
$element = $elements->item(0);
Wiki::parse_more_season_episodes($host.$element->getAttribute("href"),$series,$episodes);
}*/
