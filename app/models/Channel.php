<?php
class Channel extends ApplicationRecord{
	public static $has_many = array('seriess');
	public static $validate= array(
	array(
					'field'=>'name',
					'rule'=> array('minLength',2),
					'message'=> 'Il nome del canale è troppo corto'),
	array(
					'field'=>'name',
					'rule'=> 'isUnique',
					'message'=> 'Il canale esiste già'),
	array(
					'field'=>'name',
					'rule'=> 'alphaNumeric',
					'message'=> 'Il canale può contenere solo numeri o caratteri'),
		
	);
	static function find_or_save($name) {
		$channel = Channel::find_first_by_name($name);
		if(!$channel){
			$channel = new Channel(array("name"=>$name));
			$channel->save();//controllare se il salvataggio è avvenuto correnttamente
		}
		return $channel;
	}
	function __toString() {
		if($this->name)
		return $this->name;
		else return '';
	}
}