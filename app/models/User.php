<?php
class User extends ApplicationRecord{
	public static $has_many = array('episodes','seriess','preferences');
	public static $has_many_to_many = array("prefSeries"=>array("seriess","preferences"));
	public static $has_one = array('group');
	public static $validate= array(
	array(
				'field'=>'username',
				'rule'=>'alphaNumeric',
				'message'=>'L\'username può contenere solo caratteri e numeri'),
	array(
				'field'=>'username',
				'rule'=> array('minLength',2),
				'message'=> 'Inserire un username di lunghezza maggiore di 2'),
	array(
				'field'=>'username',
				'rule'=>'isUnique',
				'message'=>'L\'username è già usato',
				'on'=>'create'),
	array(
				'field'=>'email',
				'rule'=>'email',
				'message'=>'Inserire un email valida'),
	array(
				'field'=>'email',
				'rule'=>'isUnique',
				'message'=>'L\'email è già usata',
				'on'=>'create'),
	array(
				'field'=>'password',
				'rule'=> array('minLength',4),
				'message'=>'Inserire una password più lunga di 4 caratteri')
	);
	function beforeSave() {
		$this->password = md5($this->password);
	}
	public static function getUser($id) {
		return User::find(array("id"=>$id));
	}
	public static function authenticate($auth_field,$password) {
		if($auth_field && $password){
			$password = md5($password);
			$user = User::find(array("email"=>$auth_field,"password"=>$password,"active"=>"1"),array("first"=>true));
			if(!$user) $user = User::find(array("username"=>$auth_field,"password"=>$password,"active"=>"1"),array("first"=>true));
			return $user;
		}
		return null;
	}
	function has_preference($preference) {
		return Preference::find(array("id_user"=>$this->id,"id"=>$preference->id),array("first"=>true));
	}
}
?>