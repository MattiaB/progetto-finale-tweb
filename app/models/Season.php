<?php
class Season extends ApplicationRecord{
	public static $has_one = array('series','user');
	public static $has_many = array('episodes');//Il numero della stagione non può essere vuoto
	public static $validate= array(
	array(
					'field'=>'number_season',
					'rule'=>'numeric',
					'message'=>'Il numero della stagione deve essere un numero'),
	array(
						'field'=>'number_season',
						'rule'=>'notEmpty',
						'message'=>'Il numero della stagione non può essere vuoto'),
	array(
					'field'=>'number_episodes',
					'rule'=> 'numeric',
					'message'=> 'Il numero di episodi deve essere un numero')
	);
}