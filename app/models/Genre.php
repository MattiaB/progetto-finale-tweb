<?php
class Genre extends ApplicationRecord{
	public static $has_many_to_many = array('seriess'=>'genresSeriess');
	public static $validate= array(
	array(
					'field'=>'name',
					'rule'=> array('minLength',2),
					'message'=> 'Il nome del genere è troppo corto'),
	array(
					'field'=>'name',
					'rule'=> 'isUnique',
					'message'=> 'Il genere esiste già')
		
	);
	static function find_or_save($name) {
		$genre = Genre::find_first_by_name($name);
		if(!$genre){
			$genre = new Genre(array("name"=>$name));
			$genre->save();//controllare se il salvataggio è avvenuto correnttamente
		}
		return $genre;
	}
	function __toString() {
		return $this->name;
	}
}