<?php
class ApplicationRecord extends Record{
	public static $date_format = 'd/m/Y';
	static function find_active($cond=null,$option=null) {
		return static::find((array)$cond+array("active"=>'1'),$option);
	}
	static function find_first_active($cond,$option=null) {
		return static::find((array)$cond+array("active"=>'1'),array("first"=>true),$option);
	}
	static function find_with_page(&$page,&$num_pages,&$count,$cond=null,$option=null,$num_for_page = 20) {
		$count = static::count($cond,$option);
		$option=(array)$option;
		$num_pages = intval($count / $num_for_page);
		$down_limit = intval($page) * $num_for_page;
		$option += array("limit"=>"$down_limit, $num_for_page") ;
		return static::find($cond,$option);
	}
	function format_date($field) {
		if($this->$field){
			$date = DateTime::createFromFormat(static::$date_format, $this->$field);
			if($date) $this->$field =  $date->format('Y-m-d');
		}
	}
	function inverse_format_date($field=null,$start_format='Y-m-d') {
		if($this->$field){
			$date = DateTime::createFromFormat($start_format, $this->$field);
			if($date) $this->$field =  $date->format(static::$date_format);
		}
	}
	function link($text,$func=null, $attributes=null) {
		$class = strtolower(get_called_class());
		$url = array($class) + (array)$func;
		$url []=$this->id;
		return link_to($url,$text,$attributes);
	}
	static function to_array_id($array, $field='id') {
		$ris = array();
		foreach ((array)$array as $value) {
			$ris []= $value->$field;
		}
		return $ris;
	}
}