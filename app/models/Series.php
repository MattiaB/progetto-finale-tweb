<?php
class Series extends ApplicationRecord{
	public static $has_many = array('seasons','episodes','preferences');//L'ordine è importante per il salvataggio
	public static $has_many_to_many = array("prefUsers"=>array("users","preferences"),'genres'=>'genresSeriess','actors'=>'actorsSeriess');
	public static $has_one = array('user','channel');
	public static $validate= array(
	array(
					'field'=>'title',
					'rule'=> 'notEmpty',
					'message'=> 'Il titolo deve esistere'),
	array(
					'field'=>'no_of_seasons',
					'rule'=> 'notEmpty',
					'message'=> "Il numero delle stagioni non può essere vuoto"),
	array(
					'field'=>'no_of_seasons',
					'rule'=> 'numeric',
					'message'=> "Il numero delle stagioni deve essere un numero"),
	array(
						'field'=>'no_of_episodes',
						'rule'=> 'numeric',
						'message'=> "Il numero degli episodi deve essere un numero"),
	array(
					'field'=>'date_start',
					'rule'=> 'notEmpty',
					'message'=> "La data del campo di inizio serie non può essere vuoto"),
	array(
					'field'=>'date_start',
					'rule'=> array("date",'d/m/Y','Y-m-d'),
					'message'=> "Inserire un formato di data valido nel campo data di inizio es. 10/12/2010"),
	array(
					'field'=>'date_end',
					'rule'=> array("date",'d/m/Y','Y-m-d'),
					'message'=> "Inserire un formato di data valido nel campo data di fine es. 10/12/2010"),
	array(
					'field'=>'date_end',
					'rule'=> array("greater_of_date",'date_start'),
					'message'=> "La data di fine deve essere più grande rispetto a quella di inizio"),
	);
	function beforeSetData($name,$array_assoc) {
		if ($array_assoc){
			if($array_assoc["date_end"]== ''){
				$array_assoc["date_end"] = null;
			}
		}
		return $array_assoc;
	}
	function afterSetData() {
		$this->format_date("date_start");
		$this->format_date("date_end");
	}

	function deleteImg() {
		if($this->path_img){
			$file = "public/images/series/".$this->path_img;
			if(file_exists($file))
			unlink($file);
		}
	}
	function afterDelete() {
		$this->deleteImg();
	}
	function uploadImage(){
		if($_FILES){
			$file = multiple($_FILES);
			$name = 'img';
			$folder = "public/images/series/";
			
			if($file[$name]["error"] == UPLOAD_ERR_OK){
				if($file[$name]['type'] == 'image/png' || $file[$name]['type'] == 'image/jpeg'){
					$type = $file[$name]['type'];
					$ext = explode("/", $type);
					$ext = $ext[1];
					$name_file = md5_file($file[$name]['tmp_name']);
					$path_img = $folder.$name_file.'.'.$ext;
					$this->deleteImg();
					if(move_uploaded_file($file[$name]['tmp_name'], $path_img)){
						make_thumb($path_img, $path_img,200,0,0,300);
						$this->path_img = $name_file.'.'.$ext;
						return true;
					}else{
						$this->errors []= "Errore nel salvataggio dell'immagine";
						return false;
					}
				}else{
					$this->errors []= "Errore nel formato dell'immagine deve essere una jpeg o png";
					return false;
				}
			}
		}
		return true;
	}
	function inverse_format_date($field=null,$start_format='Y-m-d') {
		parent::inverse_format_date('date_start',$start_format);
		parent::inverse_format_date('date_end',$start_format);
	}
	function get_last_episode_broadcast() {
		return Episode::find(array("id_series"=>$this->id, "original_air_date"=>'<='.date("Y-m-d")),array("order"=>"original_air_date DESC","limit"=>"1","first"=>true));
	}
	function get_next_episode_broadcast() {
		return Episode::find(array("id_series"=>$this->id, "original_air_date"=>'>'.date("Y-m-d")),array("order"=>"original_air_date DESC","limit"=>"1","first"=>true));
	}
	function number_episodes() {
		return $this->no_of_episodes;
	}
	function is_preference($user) {
		if($user)
		return Preference::find(array("id_user"=>$user->id,"id_series"=>$this->id),array("first"=>true));
		else return false;
	}
	static function get_whith_last_episode_out() {
		$seriess = Series::find(array("active"=>'1'),array("where"=>"AND date_end IS NULL ","order"=>"rate DESC","limit"=>"6",));
		foreach ($seriess as $series) {
			$episode = Episode::find(array("id_series"=>$series->id, "original_air_date"=>'<='.date("Y-m-d")),array("order"=>"original_air_date DESC","limit"=>"1","first"=>true));
			if($episode){
				$ris []= $episode->series;
			}
		}
		return $ris;
	}

	static function find_complex_cond($cond) {
		$series=array();
		foreach ($cond as $key => $value) {
			$class = ucfirst($key);
			$ris = Genre::find_first_by_name($value);
			$genres = GenresSeries::find(array("id_genre"=>$ris->id));
			$series =array();
			foreach ($genres as $value) {
				$series []= $value->series;
			}
		}
		return $series;

	}

}
