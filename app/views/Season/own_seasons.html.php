<?php $this->partial("miniSearchBox",array("link"=>array("season","own_seasons"))) ?>
<?php echo link_to(array("season","own_seasons"), "Visualizza tutte le stagioni"); ?>
<?php echo link_to(array("season","active_all"), "Attiva tutte le stagioni",array('class'=>'noajax')); ?>
<table class="table centerbox">
	<tr>
		<th>Serie</th>
		<th>No. stagione</th>
		<th>No. Episodi</th>
		<th>Creato il</th>
		
		
		
		
	<?php if($this->isBloggerOrMore()) { ?>	<th>Attivo</th> <?php }?>
		<th>Modifica</th>
		<th>Cancella</th>
	</tr>
	
	
	
	
	<?php
	foreach ($seasons as $season) {
		echo '<tr>';
		$series = $season->series;
		echo tagClose('td',$series->link($series->title));
		echo tagClose("td",safe($season->number_season));
		echo tagClose("td",safe($season->number_episodes));
		echo tagClose("td",strDateTime(safe($season->created_at)));
		if($this->isBloggerOrMore()) {
		echo "<td>";
		if($season->active == '1')	echo link_to(array("season","deActive",$season->id), "Disattiva",array('class'=>'noajax'));
		else						echo link_to(array("season","active",$season->id), "Attiva",array('class'=>'noajax'));
		echo "</td>";
		}
		echo tagClose("td",link_to(array("season","modify",$season->id),"Modifica"));
		echo tagClose("td",link_to(array("season","delete",$season->id),"Cancella",array('class'=>'ajaxdeletelink')));
		echo '</tr>';
	}
	?>
</table>

<?php
$this->partial("pages",array("num_pages"=>$num_pages,"page"=>$page,"link"=>array("season","own_seasons")));
 ?>