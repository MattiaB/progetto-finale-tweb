<form action="<?php echo uri($url)?>" method="post" class="form">
<fieldset>
<?php echo tagClose("h3",$title);?>
	<label>Numero di stagione</label>
	
	
<?php echo formText(array("value"=>'',"name"=>"number_season","value"=>$season->number_season,'class'=>'notempty numeric','numeric'=>'Il numero della stagione deve essere un numero','notempty'=>'Il numero della stagione non può essere vuoto'))?>
<label>Numero di Episodi</label>
<?php echo formText(array("value"=>'',"name"=>"number_episodes","value"=>$season->number_episodes,'class'=>'numeric','numeric'=>'Il numero di episodi deve essere un numero'))?>

<?php echo formSubmit($submit)?>
</fieldset>
</form>
