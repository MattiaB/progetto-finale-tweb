<?php
$series = $season->series;
echo tagClose('h2',$series->link(safe($series->title)).' -'.$season->link('Stagione '.$season->number_season).' - Episodi '.$season->number_episodes);
if($this->isUserOrMore()){
	echo link_to(array("episode","create",$series->id,$season->id), "Aggiungi episodio",array("class"=>"margin1 block"));
	echo link_to(array("season","modify",$season->id), "Modifica stagione",array("class"=>"margin1 block"));
}
?>
<table class="table centerbox seasiontable">
	<tr>
		<th class="dim60">No. nella serie</th>
		<th class="dim60">No. nella stagione</th>
		<th>Titolo</th>
		<th>Data di trasmissione</th>
		<th>Voto</th>
	</tr>
	
	
	
		
<?php 
foreach ($episodes as $value) {
	?><tr><?php 
	echo tagClose('td',$value->no_in_series);
	echo tagClose('td',$value->no_in_season);
	echo tagClose('td',$value->link(safe($value->title)),true);
	echo tagClose('td',strDate($value->original_air_date));
	echo tagClose('td',$value->rate);
	?></tr><?php
}?>
</table>
