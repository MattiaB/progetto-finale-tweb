<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Series Premiere</title>
<?php 
echo css("reset.css");
echo css("text.css");
echo css("layout.css");
echo css("design.css");
echo script("jquery.min.js");
echo script("login.js");
echo script("search.js");
echo script("validation.js");
echo script("ajax.js");
?>

</head>
<body>

	<div id="container">
		<div id="header" class="radius bg2">
		<?php
		echo tagClose("h1",link_to("index", "Series Premiere",array("class"=>"block")));?>
			
		<?php $this->partial("slide_login");?>
		</div>
		<div id="slide" class="floatright sidemenu">
		<?php if($this->session->user){
			$this->partial("preference");
			$this->partial("linkUser");
		}?>

		</div>
		<div id="navigation" class="floatleft sidemenu">
			
			
		<?php $this->partial("menu");?>
		</div>
		<div id="content">

			
			
		<?php
		$this->partial("message",array("class"=>"note"));
		$this->partial("message",array("class"=>"errors"));
		$this->content();
		?>
			
		</div>
		<div id="footer" class="clearboth">
			<p>2012 - Series Premiere -  La maggiorparte dei dati sono stati presi da Wikipedia</p>
		</div>
	</div>
</body>
</html>
