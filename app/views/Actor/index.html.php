<h2>Attori</h2>
<?php $this->partial("miniSearchBox",array("link"=>array("actor"))) ?>
<?php echo link_to(array("actor","create"),safe('Crea un nuovo attore'));?>
<?php echo link_to(array("actor"), "Visualizza tutti gli attori"); ?>
<table class="table centerbox">
	<tr>
		<th>Nome</th>
		<th>Numero di serie</th>
		
		
		
		

<?php if($this->isBloggerOrMore()) { ?>
		<th>Modifica</th>
		<th>Cancella</th>
		<?php }?>
	</tr>
	
	
	
	
	<?php
	foreach ((array)$actors as $actor) {
		echo '<tr>';
		$http_query = Request::static_to_http_query(array("actors[]"=>$actor->id));
		echo tagClose('td',link_to(array("search","add"=>$http_query),$actor->name));
		$count = ActorsSeries::count(array("id_actor"=>$actor->id));
		echo tagClose("td",$count);
		if($this->isBloggerOrMore()) {
			echo tagClose("td",link_to(array("actor","modify",$actor->id),"Modifica"));
			echo tagClose("td",link_to(array("actor","delete",$actor->id),"Cancella",array('class'=>'ajaxdeletelink')));
		}
		echo '</tr>';
	}
	?>
</table>



<?php
$this->partial("pages",array("num_pages"=>$num_pages,"page"=>$page,"link"=>array("actor")));
 ?>