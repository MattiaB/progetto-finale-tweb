<?php if($num_pages){ ?>
<div class="pages">
<?php
//echo tagClose("p","Numero di Pagine $num_pages");
$index_page = count($link);
if(!$this->get->isEmpty()){
	$link += array('add'=> $this->get->to_http_query());
}
?>
	<div class="beginlink">
	<?php
	if($page){
		$link[$index_page]= 0;
		echo link_to($link ,"Prima",array("class"=>"biglink"));

		$link[$index_page] =($page-1);
		echo link_to($link,"Precedente",array("class"=>"biglink"));

	}
	?></div>
	<?php
$range = 5;
$begin = 0;
if($page > $range){
	$begin = $page - $range;
}
$end = $num_pages;
?><div class="middlelink"><?php
if($page + $range < $num_pages ){
	$end= $page + $range;
}
for ($i = $begin; $i <= $end; $i++) {
	if($page != $i){
		$link[$index_page] =($i);
		echo link_to($link,"$i",array("class"=>"pagelink"));
	}else{
		echo tagClose("span","$i",array("class"=>"pagelink"));
	}
}
?></div><?php
?><div class="lastlink"><?php
if($page != $num_pages){

	$link[$index_page] =($page+1);
	echo link_to($link ,"Prossima",array("class"=>"biglink"));
	
	$link[$index_page]= $num_pages;
	echo link_to($link ,"Ultima",array("class"=>"biglink"));
}
?></div><?php
?>
</div>




<?php } ?>
