<?php
/* Varibili da passare sono $label, $filter, $class, $name, [$series]*/
?>
<div class="boxform filter">
	<label><?php echo $label ?> </label>
	
	
	
	
<?php echo formInput("text",array("title"=>$filter));?>
<div class="boxcheckbox">
<?php 
$values = null;
if(isset($series))
	$values = array_ids($series->$name);
foreach ($class::all(array("order"=>"name ASC")) as $value) {?>
		<div >
		<?php 
		$attributes = array("name"=>$name."[]","value"=>$value->id);
		if($values && in_array($value->id,$values)){
			$attributes += array("checked"=>"yes");
		}
		echo formInput("checkbox",$attributes);
		echo "<span>".$value->name."</span>";
		?>
		</div>
		<?php
}?>
</div>
</div>
