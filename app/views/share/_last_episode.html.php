<?php
$episode_last = $series->get_last_episode_broadcast();
$episode_next = $series->get_next_episode_broadcast();
if(!$series->date_end && $episode_last){
	echo tagClose('span','Ultimo episodio:',array("class"=>"heading"));
	echo tagClose('span',$episode_last->season->number_season.'x'.$episode_last->no_in_season.' '.$episode_last->link($episode_last->title).' - '.strDate($episode_last->original_air_date),array("class"=>"epdata"));
}if( $episode_next){
	echo tagClose('span','Prossimo episodio:',array("class"=>"heading"));
	echo tagClose('span',$episode_next->season->number_season.'x'.$episode_next->no_in_season.' '.$episode_next->link($episode_next->title).' - '.strDate($episode_next->original_air_date),array("class"=>"epdata"));
}