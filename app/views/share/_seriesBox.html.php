<div class="box series">
<?php
?>
	<div class="img">
	<?php
	echo $series->link(img('series/'.$series->path_img,array("alt"=>$series->title)));
	?></div>
	
	<?php
	?><div class="data"><?php
	echo tagClose('h3',$series->link($series->title));
	echo tagClose('span','Voto:',array("class"=>"heading"));
	echo tagClose('span',$series->rate,array("class"=>"epdata"));
	echo tagClose('span','Numero stagioni:',array("class"=>"heading"));
	echo tagClose('span',$series->no_of_seasons,array("class"=>"epdata"));
	echo tagClose('span','Numero episodi:',array("class"=>"heading"));
	echo tagClose('span',$series->no_of_episodes,array("class"=>"epdata"));
	$this->partial("last_episode",array("series"=>$series));
	if($series->date_end){
		echo tagClose('span','Data inizio:',array("class"=>"heading"));
		echo tagClose('span',strDate($series->date_start),array("class"=>"epdata"));
		echo tagClose('span','Data fine:',array("class"=>"heading"));
		echo tagClose('span',strDate($series->date_end),array("class"=>"epdata"));
	}
	$this->partial("linksUserSeries",array("series"=>$series));
	?></div><?php
	?></div>
<?php
