<div class="radius slideNav" id="preferences">
	<h3>Serie preferite</h3>
	
	
<?php
if(!count($this->session->user->preferences)) echo tagClose("p","Inserisci le tue serie preferite");
foreach ((array)$this->session->user->preferences as $preference) {
	echo '<div class="preference">';
	$series = $preference->series;
	$episode = $preference->episode;
	echo link_to(array("series",$series->id,),$series->title,array("class"=>"prefSeries"));
	if($episode)
	echo link_to(array("episode",$episode->id),$episode->title,array("class"=>"prefEp"));

	echo '</div>';
}
?>
</div>
