<?php
if($this->isUserOrMore()){
	$pref = $series->is_preference($this->session->user);
	if(!$pref)
	echo link_to(array("users","add_preference",$series->id),"Aggiugi serie tra le predefinite",array('class'=>'noajax'));
	else{
		if($pref->newsletter == '0')
		echo link_to(array("users","newsletter_on",$pref->id),"Iscriviti alla newsletter",array('class'=>'noajax'));
		else
		echo link_to(array("users","newsletter_off",$pref->id),"Cancella l'iscrizione alla newsletter",array('class'=>'noajax'));
		echo link_to(array("users","remove_preference",$pref->id),"Rimuovi la serie dai predefiniti",array('class'=>'noajax'));
	}
}