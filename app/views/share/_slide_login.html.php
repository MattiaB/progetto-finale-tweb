<div class="slidelogin">

<?php
if(!$this->session->user){
	$this->partial("login");
	echo link_to(array("users","create"), "Iscriviti",array("class"=>"textwhite sign-in"));
}else{
	echo tagClose("span",safe("È stato effettuato l'accesso come:"),array("class"=>"block access"));
	echo tagClose("span",safe($this->session->user->username),array("class"=>"textcenter strong block username"));
	echo tagClose("span","Email: ",array("class"=>"email"));
	echo tagClose("span",safe($this->session->user->email),array("class"=>"strong loginemail"));
	echo link_to(array("sessions","destroy"), "Esci",array("class"=>"block textwhite sign-out noajax"));
}?>
</div>
