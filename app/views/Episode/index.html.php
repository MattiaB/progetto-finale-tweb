<?php
$series = $episode->series;
$season = $episode->season;
$titleSeries = link_to(array("series",$series->id),safe($series->title));
$titleSeason = link_to(array("season",$season->id),safe('Stagione '.$season->number_season));
echo tagClose("h3","$titleSeries - $titleSeason - ".$episode->title);
echo tagClose("p","Numero puntata nella serie: ".$episode->no_in_series);
echo tagClose("p","Numero puntata nella stagione: ".$episode->no_in_season);
if($episode->user){
	echo tagClose("p","Creato da: ".$episode->user->username);
}else {
	echo tagClose("p","L'utente non esiste");
}
echo tagClose("p",safe($episode->description));
echo tagClose("p","Data originale di trasmissione: ".strDate($episode->original_air_date));
echo tagClose("p","Voto: ".$episode->rate);
if($this->isUserOrMore()){
	?>

<form action="<?php echo uri(array("users","vote",$episode->id))?>"
	method="post" class="form vote">
	<label>Vota l'episodio</label>
	
	
	
	
<?php
$i=1;
while($i<=5){
	echo formInput("input",array("type"=>"radio","name"=>"rate","value"=>$i));
	?><span><?php echo "$i Stella"?></span><?php 
	$i++;
}
echo formSubmit("Vota Episodio");
?>
</form>

<?php
if($episode->series->is_preference($this->session->user)){
	echo link_to(array("users","add_last_episode",$episode->id),"Ultimo episodio visto della serie",array("class"=>"margin1 block noajax"));
}
if($this->isUserOrMore($episode)){
	echo link_to(array("episode","modify",$episode->id),"Modifica",array("class"=>"margin1 block"));
	}
}
?>
