<form action="<?php echo uri($url)?>" method="post" class="form">
<fieldset>
<?php echo tagClose("h3",$title);?>
	<label>Titolo</label>
	
	
<?php echo formText(array("name"=>"title","value"=>$episode->title,'length'=>'2','minlength'=>'Il titolo è troppo corto deve essere più lungo di due caratteri'))?>
<label>Descrizione</label>
<?php echo tagClose("textarea",safe($episode->description),array("name"=>"description"))?>
<label>Numero Puntata nella Serie</label>
<?php echo formText(array("name"=>"no_in_series","value"=>$episode->no_in_series,'class'=>'notempty numeric','numeric'=>'Il numero dell\'episodio nella serie deve essere un numero','notempty'=>'Il numero dell\'episodio deve essere presente'))?>
<label>Numero Puntata nella Stagione</label>
<?php echo formText(array("name"=>"no_in_season","value"=>$episode->no_in_season,'class'=>'numeric','numeric'=>'Il numero dell\'episodio nella stagione deve essere un numero'))?>

<label>Data di trasmissione</label>
<?php echo formText(array("name"=>"original_air_date","title"=>"es. 10/11/2011","value"=>strdate($episode->original_air_date,'d/m/Y'),'class'=>'date','regexp'=>'^\d{2}\/\d{2}\/\d{4}','date'=>'Inserire un formato di data valido nel campo data di trasmissione es. 10/12/2010'))?>
<?php echo formSubmit($submit)?>
</fieldset>
</form>
