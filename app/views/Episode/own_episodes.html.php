<?php $this->partial("miniSearchBox",array("link"=>array("episode","own_episodes"))) ?>
<?php echo link_to(array("episode","own_episodes"), "Visualizza tutti gli episodi"); ?>
<?php echo link_to(array("episode","active_all"), "Attiva tutte gli episodi",array('class'=>'noajax')); ?>
<table class="table centerbox">
	<tr>
		<th>Serie</th>
		<th>No. serie</th>
		<th>No. stagione</th>
		<th>Stagione</th>
		<th>Titolo</th>
		<th>Data di trasmissione</th>
		<th>Rate</th>
		<th>Creato il</th>
		
		
		
		
	<?php if($this->isBloggerOrMore()) { ?>	<th>Attivo</th> <?php }?>
		<th>Modifica</th>
		<th>Cancella</th>
	</tr>
	
	
	
	
	<?php
	$i=0;
	foreach ($episodes as $episode) {
		echo '<tr>';
		$series = $episode->series;
		$season = $episode->season;
		if($series)
		echo tagClose('td',link_to(array("series",$series->id),$series->title));
		echo tagClose("td",safe($episode->no_in_series));
		echo tagClose("td",safe($episode->no_in_season));
		if($season)
		echo tagClose("td",safe($season->number_season));
		echo tagClose("td",safe($episode->title));
		echo tagClose("td",strDate(safe($episode->original_air_date)));
		echo tagClose("td",safe($episode->rate));
		echo tagClose("td",strDateTime(safe($episode->created_at)));
		if($this->isBloggerOrMore()) {
		echo "<td>";
		if($episode->active == '1')	echo link_to(array("episode","deActive",$episode->id), "Disattiva",array('class'=>'noajax'));
		else						echo link_to(array("episode","active",$episode->id), "Attiva",array('class'=>'noajax'));
		echo "</td>";
		}
		echo tagClose("td",link_to(array("episode","modify",$episode->id),"Modifica"));
		echo tagClose("td",link_to(array("episode","delete",$episode->id),"Cancella",array('class'=>'ajaxdeletelink')));
		echo '</tr>';
		if($i == 40) break;
		$i++;
	}
	?>
</table>

<?php

$this->partial("pages",array("num_pages"=>$num_pages,"page"=>$page,"link"=>array("episode","own_episodes")));
 ?>