<form action="<?php echo uri($url)?>" method="post" class="form">
<fieldset>
<?php echo tagClose("h3",$title); ?>
	<label>Username</label>
	
	
<?php echo formText(array("value"=>$user->username,"name"=>"username",'class'=>'minlength alphanumeric','length'=>'2','alphanumeric'=>"L'username può contenere solo caratteri e numeri",'minlength'=>'Inserire un username di lunghezza maggiore di 2'))?>
<label>E-Mail</label>
<?php echo formText(array("value"=>$user->email,"name"=>"email","class"=>"email",'email'=>'Inserire un email valida'))?>
<label>Password</label>
<?php echo formInput("password",array("name"=>"password"))?>
<label>Gruppo</label>
<select name="id_group">
<?php foreach (Group::all() as $group) {
	if($group->id != $user->id_group)
		echo tagClose("option",$group->type,array("name"=>"id_group", "value"=>$group->id));
	else 
		echo tagClose("option",$group->type,array("name"=>"id_group", "value"=>$group->id,"selected"=>"selected"));
}?>
</select>

<?php echo formSubmit($submit)?>
</fieldset>
</form>
