<table class="table centerbox">
	<tr>
		<th>Username</th>
		<th>E-mail</th>
		<th>Created At</th>
		<th>Active</th>
		<th>Group</th>
		<th>Modify</th>
		<th>Delete</th>
	</tr>
	
	
<?php
foreach ($users as $user) {
	echo '<tr>';
	echo "<td>$user->username</td>";
	echo "<td>$user->email</td>";
	echo "<td>$user->created_at</td>";
	echo "<td>";
	if($user->active == '1')	echo link_to(array("admin","deActiveUser",$user->id), "DeActive",array('class'=>'noajax'));
	else						echo link_to(array("admin","activeUser",$user->id), "Active",array('class'=>'noajax'));
	echo "</td>";
	$group_name = $user->group->type;
	echo "<td>$group_name</td>";
	echo "<td>".link_to(array("admin","modifyUser",$user->id), "Modify")."</td>";
	echo "<td>".link_to(array("admin","deleteUser",$user->id), "Delete",array('class'=>'ajaxdeletelink'))."</td>";
	echo '</tr>';
}
?>
</table>
