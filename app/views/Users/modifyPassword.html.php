<form action="<?php echo uri(array("users","modifyPassword"))?>"
	method="post" class="form">
	<fieldset>
	<?php echo  tagClose("h3",$this->title);?>

	<?php echo tagClose("label","Vecchia Password")?>

	<?php echo formInput("password",array("name"=>"oldPassword"))?>

	<?php echo tagClose("label","Nuova Password")?>

	<?php echo formInput("password",array("name"=>"password","class"=>"minlength",'length'=>'4','minlength'=>'Inserire una password più lunga di 4 caratteri'))?>

	<?php echo tagClose("label","Conferma Password")?>

	<?php echo formInput("password",array("name"=>"confirmPassword"))?>

	<?php echo formSubmit("Modifica Password")?>
	</fieldset>
</form>
