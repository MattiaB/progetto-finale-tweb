<form action="<?php echo uri(array("users","create"))?>" method="post"
	class="form">
	<fieldset>
	<?php echo  tagClose("h3",$this->title);?>

	<?php echo tagClose("label","Username:")?>

	<?php echo formText(array("name"=>"username","value"=>$this->user->username,'class'=>'minlength alphanumeric','length'=>'2','alphanumeric'=>"L'username può contenere solo caratteri e numeri",'minlength'=>'Inserire un username di lunghezza maggiore di 2'))?>

	<?php echo tagClose("label","Email:")?>

	<?php echo formText(array("name"=>"email","value"=>$this->user->email,"class"=>"email",'email'=>'Inserire un email valida'))?>

	<?php echo tagClose("label","Password")?>

	<?php echo formInput("password",array("name"=>"password","class"=>"minlength",'length'=>'4','minlength'=>'Inserire una password più lunga di 4 caratteri'))?>

	<?php echo formSubmit("Iscriviti")?>
	</fieldset>
</form>
