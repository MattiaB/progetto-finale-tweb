<form action="<?php echo uri(array("search")) ?>" method="get"
	class="form search">
	<fieldset>
	<div class="optionbox">
		<h3>Ricerca Avanzata</h3>
		<div class="searchGeneric">
			<label>Cerca questa parola</label>
			
				
<?php echo formInput("text",array("name"=>"generic","class"=>"textSearch","autocomplete"=>"off"));?>
</div>
		<div class="boxform submit">
			
		<?php echo formSubmit("Cerca"); ?>
		</div>
		
		
<?php 
$this->partial("boxcheck",array("label"=>"Generi","filter"=>"Filtra i generi","class"=>"Genre","name"=>"genres"));
$this->partial("boxcheck",array("label"=>"Attori","filter"=>"Filtra gli attori","class"=>"Actor","name"=>"actors"));
$this->partial("boxcheck",array("label"=>"Canali","filter"=>"Filtra i canali","class"=>"Channel","name"=>"channels"));
?>
<div class="boxform">
<label>Voto</label>
<div class="boxcheckbox">
<?php 
for ($i = 1; $i <= 5; $i++) {?>
	<div >
	<?php 
	echo formInput("checkbox",array("name"=>"votes[]","value"=>$i));
	echo "<span>Stella $i</span>";?>
	</div>
<?php } ?>
	</div>
</div>

<div class="boxform">
<label>In corso o non</label>
<div class="boxcheckbox">
<div >
<?php 
echo formInput("checkbox",array("name"=>"continuously[]", "value"=>"now"));
echo "<span>In corso</span>";
?></div><div ><?php 
echo formInput("checkbox",array("name"=>"continuously[]", "value"=>"end"));
echo "<span>Non in corso</span>";
?>
</div>
</div>
</div>

<div class="boxform">
<label>Ordina per </label>
<div class="boxcheckbox">
<div >

<select name="order">
	<?php echo tagClose("option","Alfabetico",array("value"=>"title"));?>
  <?php echo tagClose("option","Alfabetico inverso",array("value"=>"!title"));?>
  <?php echo tagClose("option",safe("Voto più alto"),array("value"=>"!rate"));?>
   <?php echo tagClose("option",safe("Voto più basso"),array("value"=>"rate"));?>
</select>

</div>
</div>
</div>


</div>

	<div class="boxform submit">

	<?php echo formSubmit("Cerca"); ?>
	</div>
</fieldset>
</form>
