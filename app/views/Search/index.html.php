<div class="infoSearch">
<?php
if(isset($seriess) || isset($episodes)){

	$string = ($count_episodes+$count_seriess).' risultati';
	if($count_seriess){
		$string .= " - $count_seriess serie";
	}
	if($count_episodes){
		if($count_episodes == 1)$string .= " - $count_episodes episodio";
		else	$string .= " - $count_episodes episodi";
	}
	echo tagClose("p",safe($string));

	if(isset($search_string))
	echo tagClose("p","Stringa cercata: ".$search_string);

	if($count_genres){
		$geners = implode(", ", $genres);
		if($count_genres == 1){
			$string = 'genere';
		}elseif ($count_genres > 1){
			$string = 'generi';
		}
		echo tagClose("p","$count_genres $string: $geners");
	}
	if($count_actors){
		$actors = implode(", ", $actors);
		if($count_actors == 1){
			$string = 'attore';
		}elseif ($count_actors > 1){
			$string = 'attori';
		}
		echo tagClose("p","$count_actors $string: $actors");
	}
	if($count_channels){
		$channels = implode(", ", $channels);
		if($count_channels == 1){
			$string = 'canale';
		}elseif ($count_channels > 1){
			$string = 'canali';
		}
		echo tagClose("p","$count_channels $string: $channels");
	}
	?>
</div>

<?php
foreach ((array)$seriess as $series) {
	$this->partial("seriesBox",array("series"=>$series));
}
if(isset($episodes)){
	foreach ((array)$episodes as $episode) {
		$series = $episode->series;
		?>
<div class="box series">
	<div class="img">




	<?php echo link_to(array("series",$series->id),img('series/'.$series->path_img,array("alt"=>$series->title)));?>
	</div>
	<div class="data">


	<?php
	echo tagClose('h3',$series->link($series->title).' - '.$episode->link($episode->title));
	echo tagClose('span','Voto:',array("class"=>"heading"));
	echo tagClose('span',$episode->rate,array("class"=>"epdata"));

	echo tagClose('span','Stagione:',array("class"=>"heading"));
	echo tagClose('span',$episode->season->number_season,array("class"=>"epdata"));

	echo tagClose('span','No. nella stagione:',array("class"=>"heading"));
	echo tagClose('span',$episode->no_in_season,array("class"=>"epdata"));

	echo tagClose('span','No. nella serie:',array("class"=>"heading"));
	echo tagClose('span',$episode->no_in_series,array("class"=>"epdata"));

	echo tagClose('span','Trasmesso il:',array("class"=>"heading"));
	echo tagClose('span',strDate($episode->original_air_date),array("class"=>"epdata"));
	if($episode->series->is_preference($this->session->user)){
		echo link_to(array("users","add_last_episode",$episode->id),"Ultimo episodio visto della serie",array("class"=>"margin1 block"));
	}
	?>
	</div>
</div>

<?php }
}
$this->partial("pages",array("num_pages"=>$num_pages,"page"=>$page,"link"=>array("search")));

 }
?>