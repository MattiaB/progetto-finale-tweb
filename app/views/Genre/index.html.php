<h2>Generi</h2>
<?php $this->partial("miniSearchBox",array("link"=>array("genre"))) ?>
<?php echo link_to(array("genre","create"),safe('Crea un nuovo genere'));?>
<?php echo link_to(array("genre"), "Visualizza tutti i generi"); ?>
<table class="table centerbox">
	<tr>
		<th>Nome</th>
		<th>Numero di serie</th>
		
		
		
		

<?php if($this->isBloggerOrMore()) { ?>
		<th>Modifica</th>
		<th>Cancella</th>
		<?php }?>
	</tr>
	
	
	
	
	<?php
	foreach ((array)$genres as $genre) {
		echo '<tr>';
		$http_query = Request::static_to_http_query(array("genres[]"=>$genre->id));
		echo tagClose('td',link_to(array("search","add"=>$http_query),$genre->name));
		$count = GenresSeries::count(array("id_genre"=>$genre->id));
		echo tagClose("td",$count);
		if($this->isBloggerOrMore()) {
			echo tagClose("td",link_to(array("genre","modify",$genre->id),"Modifica"));
			echo tagClose("td",link_to(array("genre","delete",$genre->id),"Cancella",array('class'=>'ajaxdeletelink')));
		}
		echo '</tr>';
	}
	?>
</table>






<?php
$this->partial("pages",array("num_pages"=>$num_pages,"page"=>$page,"link"=>array("genre")));
 ?>