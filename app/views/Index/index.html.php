
<?php
$seriess = Series::get_whith_last_episode_out();
?>
<div class="centerbox dimImg indexdivImgs">
<?php
foreach ($seriess as $series) {
	?>
	<div class="indexdivImg">
	<?php
	echo tagClose("h3",$series->title, array("class"=>"displaynone dimImg")) ;
	echo link_to(array("series",$series->id), img('series/'.$series->path_img,array("class"=>"block dimImg radius indexImg","alt"=>$series->title)));
	$last_episode =$series->get_last_episode_broadcast();
	?>
		<div class="indeximgspan">
		<?php
		echo tagClose("span",$last_episode->link($last_episode->season->number_season.'x'.$last_episode->no_in_season.' - '.$last_episode->title), array("class"=>"displaynone dimImg indexserieshover"));
		?></div>
		
		<?php
	?></div>
	
	<?php
}?>
</div>
