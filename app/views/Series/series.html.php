<?php
echo tagClose('h2',$series->title,array("class"=>"block"));
$actors=series_actors($series);
$genres=series_genres($series);
$channel = series_channel($series);
$original_run= series_date($series);
$number_episodes = $series->no_of_episodes;
?>
<table class="table info">
	<tr>
		<td colspan="2"><?php echo img('series/'.$series->path_img,array("class"=>"","alt"=>$series->title))?>
		</td>
	</tr>
	<tr>
		<th>Voto</th>
		<td><?php echo $series->rate?></td>
	</tr>
	<tr>
		<th>Generi</th>
		<td><?php echo $genres?></td>
	</tr>
	<tr>
		<th>Attori principali</th>
		<td><?php echo $actors?></td>
	</tr>
	<tr>
		<th>Numero stagioni</th>
		<td><?php echo $series->no_of_seasons?></td>
	</tr>
	<tr>
		<th>Numero Episodi</th>
		<td><?php echo $number_episodes?></td>
	</tr>
	<tr>
		<th>Canale originale</th>
		<td><?php echo $channel?></td>
	</tr>
	<tr>
		<th>Data di trasmissione</th>
		<td><?php echo $original_run?>
		</td>
	</tr>
</table>
<?php
echo tagClose('p',$series->description,array("class"=>"description"));
$this->partial("last_episode",array("series"=>$series));
echo tagClose('span','Creato da:',array("class"=>"heading"));
if($series->user){
	echo tagClose('span',$series->user->username);
}else{
	echo tagClose('span',"L'utente non esiste");
}
foreach (Season::find(array("id_series"=>$series->id),array("order"=>"number_season ASC")) as $seasion) {
	echo link_to(array("season",$seasion->id), "Stagione ".$seasion->number_season,array("class"=>"margin1 block"));
}
?>
<div class="userlink">
<?php
$this->partial("linksUserSeries",array("series"=>$series));
if($this->isUserOrMore()){
	echo link_to(array("season","create",$series->id), "Aggiungi stagione");
}
if($this->isUserOrMore($series)){
	echo link_to(array("series","modify",$series->id), "Modificare Serie");
}?>
</div>
