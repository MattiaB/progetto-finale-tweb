
<form action="<?php echo uri($url)?>" method="post"
	enctype="multipart/form-data" class="form">
	<fieldset>
	<?php echo tagClose("h3",$title);?>
	<label>Titolo</label>
	
	
	
	
<?php echo formText(array("name"=>"title","value"=>$series->title,'class'=>'notempty','notempty'=>'Il titolo deve esistere'))?>
<label>Descrizione</label>
<?php echo tagClose("textarea",$series->description,array("name"=>"description","rows"=>'10',"cols"=>"10"))?>
<label>Numero di Stagioni</label>
<?php echo formText(array("name"=>"no_of_seasons","value"=>$series->no_of_seasons,'class'=>'notempty numeric','numeric'=>'Il numero delle stagioni deve essere un numero','notempty'=>'Il numero delle stagioni non può essere vuoto'))?>
<label>Numero di Episodi</label>
<?php echo formText(array("name"=>"no_of_episodes","value"=>$series->no_of_episodes,'class'=>'numeric','numeric'=>'Il numero degli episodi deve essere un numero'))?>
<label>Anno di Inizio</label>
<?php echo formText(array("name"=>"date_start","title"=>"es. 10/11/2011","value"=>$series->date_start,'class'=>'notempty date','regexp'=>'^\d{2}\/\d{2}\/\d{4}','date'=>'Inserire un formato di data valido nel campo data di inizio es. 10/12/2010','notempty'=>'La data del campo di inizio serie non può essere vuoto'))?>
<label>Anno di Fine</label>
<?php echo formText(array("name"=>"date_end","title"=>"es. 10/11/2011","value"=>$series->date_end,'class'=>'date','regexp'=>'^\d{2}\/\d{2}\/\d{4}','date'=>'Inserire un formato di data valido nel campo data di fine es. 10/12/2010'))?>
<?php 
$this->partial("boxcheck",array("label"=>"Generi","filter"=>"Filtra i generi","class"=>"Genre","name"=>"genres","series"=>$series));
$this->partial("boxcheck",array("label"=>"Attori","filter"=>"Filtra gli attori","class"=>"Actor","name"=>"actors","series"=>$series));
?>

<label>Canale originale</label>
<select name="id_channel">
<?php foreach (Channel::all() as $channel) {
	if( $series->channel && $channel->id == $series->channel->id){
		echo tagClose("option",$channel->name,array("value"=>$channel->id,"selected"=>"selected"));
	}
	else{
		echo tagClose("option",$channel->name,array("value"=>$channel->id));
	}
}?>
</select>
<label>Immagine serie</label>
<?php echo formInput("file",array("name"=>"img"))?>

<?php echo formSubmit($submit)?>
</fieldset>
</form>
