<?php $this->partial("miniSearchBox",array("link"=>array("series","own_series"))) ?>
<?php echo link_to(array("series","own_series"), "Visualizza tutte le serie"); ?>
<?php echo link_to(array("series","active_all"), "Attiva tutte le serie",array('class'=>'noajax')); ?>
<table class="table centerbox">
	<tr>
		<th>Titolo</th>
		<th>No. Stagioni</th>
		<th>No. Episodi</th>
		<th>Data inizio</th>
		<th>Data fine</th>
		<th>Utente</th>
		<th>Creato il</th>
	<?php if($this->isBloggerOrMore()) { ?>	<th>Attivo</th> <?php }?>
		<th>Modifica</th>
		<th>Cancella</th>
	</tr>
	<?php
	foreach ((array)$seriess as $series) {
		echo '<tr>';
		echo tagClose('td',link_to(array("series",$series->id),$series->title));
		echo tagClose("td",$series->no_of_seasons);
		echo tagClose("td",$series->no_of_episodes);
		echo tagClose("td",strDate($series->date_start));
		if($series->date_end == null)
			echo tagClose("td","Adesso");
		else
			echo tagClose("td",strDate($series->date_end));
		echo tagClose("td",safe($series->user->username));
		echo tagClose("td",strDateTime($series->created_at));
		if($this->isBloggerOrMore()){
		echo "<td>";
		if($series->active == '1')	echo link_to(array("series","deActive",$series->id), "Disattiva",array('class'=>'noajax'));
		else						echo link_to(array("series","active",$series->id), "Attiva",array('class'=>'noajax'));
		echo "</td>";
		 }
		echo tagClose("td",link_to(array("series","modify",$series->id),"Modifica"));
		echo tagClose("td",link_to(array("series","delete",$series->id),"Cancella",array('class'=>'ajaxdeletelink')));
		echo '</tr>';
	}
	?>
</table>

<?php
$this->partial("pages",array("num_pages"=>$num_pages,"page"=>$page,"link"=>array("series","own_series")));
 ?>