<?php
class TestController extends ApplicationController{
	function testRelations() {
		$this->user = User::find(array('id'=>1));
		$this->series = Series::find(array('id'=>1));
		$this->episode = Episode::find(array('id'=>1));
		$this->actor = Actor::find(array('id'=>1));
		$this->genre = Genre::find(array('id'=>1));
		$this->group = Group::find(array('id'=>1));
		$this->channel = Channel::find(array('id'=>1));
		$this->preference = Preference::find(array('id'=>1));
		$this->seasion = Seasion::find(array('id'=>1));

		$this->view();
	}
	function testInsertSeries() {
		if(!$this->post->isEmpty()){
			Wiki::parse_series($this->post->link);
		}
		$this->view();
	}
	function testInsertSeasions($series_id) {
		$this->series_id = $series_id;
		if(!$this->post->isEmpty()){
			Wiki::parse_seasions($this->post->link,$series_id);
		}
		$this->view();
	}
	function testInsertEpisodes($series_id,$seasion_id=0) {
		$this->series_id = $series_id;
		$this->seasion_id = $seasion_id;
		if(!$this->post->isEmpty()){
			Wiki::parse_episodes($this->post->link,$series_id,$seasion_id);
		}
		$this->view();
	}
}