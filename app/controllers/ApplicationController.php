<?php
class ApplicationController extends Controller{
	protected function isAdmin() {
		if($user = $this->session->user){
			if($user->group->code == 0){
				return true;
			}
		}
		return false;
	}
	protected function isBloggerOrMore() {
		if($user = $this->session->user){
			if($user->group->code <= 1){
				return true;
			}
		}
		return false;
	}
	protected function isUserOrMore($obj=null) {
		if($user = $this->session->user){
			if($user->group->code <= 2){
				return true;
			}else{
				if($obj){
					return $user->id == $obj->user->id;
				}
				return true;
			}
		}
		return false;
	}
	protected function user() {
		return $this->session->user;
	}
	protected function userAuth() {
		if(!$this->isUserOrMore()){
			$this->flash["errors"] = "Bisogna iscriversi al sito per accedere a questa sezione";
			$this->redirectTo("referer");
		}
	}
	protected function bloggerAuth() {
		if(!$this->isBloggerOrMore()){
			$this->flash["errors"] = "Accesso non permesso";
			$this->redirectTo("referer");
		}
	}
	protected function adminAuth() {
		if(!$this->isAdmin()){
			$this->flash["errors"] = "Accesso non permesso";
			$this->redirectTo("referer");
		}
	}
	protected function beforeRedirectToRefer() {
		if($this->isAjax()){
			$this->partial("message",array("class"=>"note"));
			$this->partial("message",array("class"=>"errors"));
			exit;
		}
	}
	protected function genericActive($id,$class, $name,$value='1') {
		$obj = $class::find_first_by_id($id);
		if($obj){
			$obj->active=$value;
			$obj->update();
		}
		$this->redirectTo("referer");
	}
} ?>