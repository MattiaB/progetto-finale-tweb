<?php
class AdminController extends UsersController{

	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array("adminAuth"=>
		array("users","createUser","modifyUser","activeUser","deActiveUser"));
	}
	function users() {
		$this-> users = User::all(array("order"=>"created_at DESC"));
		$this->view();
	}
	function createUser() {
		$this->url = array("admin","createUser");
		$this->user = new User();
		if(!$this->post->isEmpty()){
			$this->user->setData($this->post);
			$this->user->active = 1;
			if($this->user->save()){
				$this->flash["note"] = "Utente ".$this->user->username.' creato';
				$this->redirectTo(array("admin","users"));
			}else{
				$this->flash["errors"]=$this->user->errors;
				
			}
			
		}
		$this->view();
	}
	function modifyUser($id) {
		$this->user = User::find_by_id($id);
		$this->url =array("admin","modifyUser",$id);

		if(!$this->post->isEmpty()){
			if($this->post->password == ''){
				unset($this->post['password']);
			}else {
				$this->post->password = md5($this->post->password);
			}
			$this->user->setData($this->post);

			if($this->user->update()){
				$this->flash["note"] = "Utente ".$this->user->username.' modificato';
				$this->redirectTo(array("admin","users"));
			}else{
				$this->flash["errors"]=$this->user->errors;
			}

		}
		$this->view();
	}
	function deleteUser($id) {
		$this->user = User::find_by_id($id);
		if($this->user){
			if($this->user->delete()){
				$this->flash["note"] = "Utente ".$this->user->username.' canellato';
			}else{
				$this->flash["errors"]=$this->user->errors;
			}
		}
		$this->redirectTo('referer');
	}
	function activeUser($id) {
		$user = User::find_by_id($id);
		$user->active = 1;
		$user->update();
		$this->redirectTo('referer');
	}
	function deActiveUser($id) {
		$user = User::find_by_id($id);
		$user->active = 0;
		$user->update();
		$this->redirectTo('referer');
	}
}