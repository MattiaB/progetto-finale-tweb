<?php
class SeasonController extends ApplicationController{
	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array(
		"modifyAuth"=>array("modify"),
		"userAuth"=>array("create","own_seasons"),
		"bloggerAuth"=>array("delete","active","active_all","deActive","active"),
		"adminAuth"=>array("create_with_wiki"));
	}
	function modifyAuth() {
		$args = func_get_args();
		$season_id = array_shift($args[2]);
		$season = Season::find_first_by_id($season_id);
		if($this->isUserOrMore($season)){
			return true;
		}else{
			$this->flash["errors"] = "Accesso non permesso";
			$this->redirectTo("referer");
		}
	}
	function index($season_id) {
		$this->season = Season::find_first_by_id($season_id);
		if(!$this->isUserOrMore($this->season)){
			if($this->season->active != '1'){
				$this->season = null;
			}

		}
		if($this->season && $this->isBloggerOrMore()){
			$this->episodes = Episode::find(array("id_season"=>$this->season->id),array("order"=>"no_in_series ASC"));
		}else{
			if($this->isUserOrMore()){
				$this->episodes = Episode::find(array("id_season"=>$this->season->id),array("where"=>"AND (id_user = ".$this->session->user->id.' OR active = '.'1) ',"order"=>"no_in_series ASC"));
			}else{
				$this->episodes = Episode::find(array("id_season"=>$this->season->id,"active"=>'1'),array("order"=>"no_in_series ASC"));
			}
		}
		if($this->season){
			$this->view();
		}else{
			$this->flash['errors'] = "La stagione non esiste";
			$this->redirectTo("referer");
		}
	}
	function create_with_wiki($series_id) {
		$this->url = array("season","create_with_wiki",$series_id);
		$this->series = Series::find_first_by_id($series_id);
		$this->season = new Season();
		$this->season->series = $this->series;
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$seasons = Wiki::parse_seasons($this->post->link);
			foreach ($seasons as $season) {
				$season->series = $this->series;
				$season->user = $this->session->user;
				$season->save();
			}
			$this->redirectTo(array("series",$this->series->id));
		}
	}
	function delete($season_id) {
		$season = Season::find_first_by_id($season_id);
		foreach ($season->episodes as $episode) {
			$episode->delete();
		}
		$season->delete();
		$this->flash["note"] = "Stagione è stata cancellata";
		$this->redirectTo('referer');
	}
	function create($series_id) {
		$this->url = array("season","create",$series_id);
		$this->series = Series::find_first_by_id($series_id);
		$this->season = new Season();
		$this->season->series = $this->series;
		$this->season->user =  $this->session->user;
		$this->title = "Aggiungi stagione alla serie ".$this->season->series->title;
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->season->setData($this->post);
			if($this->season->save()){
				$this->flash["note"] = "Stagione creata";
				$this->redirectTo(array("season",$this->season->id));
			}else{
				$this->flash["errors"]=$this->season->errors;
				$this->view();
			}
		}
	}
	function modify($season_id) {
		$this->url = array("season","modify",$season_id);
		$this->season = Season::find_first_by_id($season_id);
		$this->title = "Modifica la stagione ".$this->season->number_season;
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->season->setData($this->post);
			if($this->season->update()){
				$this->flash["note"] = "Stagione modificata";
				$this->redirectTo(array("season",$this->season->id));
			}else{
				$this->session->flash["errors"]=$this->season->errors;
				$this->view();
			}
		}
	}
	function active($season_id) {
		$this->genericActive($season_id,'Season','Attiva','1');
	}
	function deActive($season_id) {
		$this->genericActive($season_id,'Season','Disattiva','0');
	}
	function active_all() {
		$seasons = Season::find(array("active"=>0));
		foreach ($seasons as $season) {
			$season->active='1';
			$season->update();
		}
		$this->redirectTo("referer");
	}
	function own_seasons($page = null) {
		$opt= array("order"=>"created_at DESC,number_season ASC");
		$cond = array();
		if(!$this->get->isEmpty()){
			$search = $this->get->generic;
			Record::safe($search);
			$opt_s = array("where"=>"title LIKE '%$search%' ","onlyIds"=>true);
			$cond_s = array();
			$series_ids = Series::find($cond_s,$opt_s);
			if($series_ids){
				$seriesId = implode(",",$series_ids);
				$opt += array("where"=>"id_series IN($seriesId) ");
			}
		}
		if(!$this->isBloggerOrMore()){
			$cond += array("id_user"=>$this->session->user->id);
		}
		$this->seasons = Season::find_with_page($page,$num_pages,$count_result,$cond,$opt);
		$this->page = $page;
		$this->num_pages = $num_pages;
		$this->view();
	}
}