<?php
class UsersController extends ApplicationController{

	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array("userAuth"=>
		array("index","modify","modifyPassword","vote","add_last_episode","remove_last_episode",
				"preferences","add_preference","remove_preference","newsletter_on","newsletter_off"));
	}
	function index() {
		$this->user = User::getUser($this->session->user->id);
		$this->view();
	}
	function modify() {
		$this->title = "Aggiorna i tuoi dati";
		$this->user = User::getUser($this->session->user->id);
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->user->setData($this->post);
			if(!$this->user->update()){
				$this->flash["errors"]=$this->user->errors;
				$this->view();
			}else{
				$this->flash["note"] = "Dati Modificati";
				$this->redirectTo(array("users"));
			}
		}
	}
	function modifyPassword() {
		$this->title = "Modifica la password";
		$this->user = User::getUser($this->session->user->id);
		if($this->post->isEmpty()){
			$this->view();
		}else{
			if($this->user->password == md5($this->post->oldPassword)){
				if($this->post->password == $this->post->confirmPassword){
					$this->user->password == md5($this->post->password);
					if($this->user->update()){
						$this->flash["note"] = "Dati Modificati";
					}else{
						$this->flash["errors"]=$this->user->errors;
					}
				}else {
					$this->flash["errors"]="La password di conferma è diversa da quella inserita";
				}
			}else{
				$this->flash["errors"]="La vecchia password è diversa da quella inserita";
			}
			$this->view();
		}
	}
	function create() {
		if($this->session->user)$this->redirectTo("index");
		$this->title = "Iscriviti Ora!";
		$this->user = new User();
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->user->setData($this->post);
			$this->user->group = Group::find(array("code"=>"3"),array("first"=>true));
			if(!$this->user->save()){
				$this->flash["errors"]=$this->user->errors;
				$this->view();
			}else{
				$this->flash["note"] = array("Grazie per la registrazione","Si prega di attendere l'attivazione da parte dell'admin");
				$this->redirectTo("index");

			}
		}
	}
	function vote($series_id) {
		$ep = Episode::find_first_by_id($series_id);
		if($ep){
			$errors = array();
			$rate = intval($this->post->rate );
			if($rate > 1 && $rate <= 5){
				$ep_rate = intval($ep->rate);
				if($ep_rate){
					$ep->rate = ($ep_rate + $rate) /2;
				}else{
					$ep->rate = $rate;
				}
				$ep->update();
				$errors += $ep->errors;
				$series = $ep->series;
				$series_rate = intval($series->rate);
				if($series_rate){
					$series->rate = ($series_rate + $rate) /2;
				}else{
					$series->rate = $rate;
				}
				$series->update();
				$errors += $series->errors;
				if(empty($errors)){
					$this->flash["note"] = "La serie è stata votata";
				}else{
					$this->flash["errors"] = $errors;
				}
			}
		}
		$this->redirectTo("referer");
	}
	function add_last_episode($episode_id){
		$ep = Episode::find_first_by_id($episode_id);
		$nextEp = Episode::find(array("no_in_series"=>$ep->no_in_series+1,"id_series"=>$ep->id_series),array("first"=>true));
		if(!$nextEp){
			$nextEp = Episode::find(array("no_in_series"=>$ep->no_in_series,"id_series"=>$ep->id_series),array("first"=>true));
		}
		if($nextEp){
			$pref = $nextEp->series->is_preference($this->session->user);
			if($pref){
				$pref->id_episode = $nextEp->id;
				if($pref->update() && $this->update_preference()){
					$this->flash["note"] = "Aggiunto ultimo episodio da vedere: ".$nextEp->title;
				}else{
					$this->flash["errors"] = $pref->errors;
				}
			}
		}
		$this->redirectTo("referer");
	}
	function remove_last_episode($episode_id){
		$ep = new Episode(array("id"=>$episode_id));
		$pref = Preference::find(array("id_user"=>$this->session->user->id,"id_episode"=>$ep->id),array("first"=>true));
		if($pref){
			$pref->id_episode = 0;
			if($pref->update() && $this->update_preference()){
				$this->flash["note"] = "Rimosso ultimo episodio da vedere: ".$nextEp->title;
			}else{
				$this->flash["errors"] = $pref->errors;
			}
		}
		$this->redirectTo("referer");
	}
	function preferences() {
		$prefs = $this->session->user->preferences;
		$series = array();
		foreach ($prefs as $pref) {
			$series []= $pref->series;
		}
		$this->series = $series;
		$this->view();
	}
	function add_preference($series_id) {
		$pref = Preference::find(array("id_user"=>$this->session->user->id,"id_series"=>$series_id),array("first"=>true));
		if(!$pref){
			$series = Series::find_first_by_id($series_id);
			$user = $this->session->user;
			$preference = new Preference();
			$preference->user = $user;
			$preference->series = $series;
			if($preference->save() &&	$this->update_preference()){
				$this->flash["note"] = "Aggiunta serie ai predefiniti".$series->title;
			}else{
				$this->flash["errors"] = $preference->errors;
			}
		}
		$this->redirectTo("referer");
	}
	function remove_preference($preference_id) {
		$preference = Preference::find_first_by_id($preference_id);
		$pref = $this->session->user->has_preference($preference);
		if($pref){
			if($pref->delete() && $this->update_preference()){
				$this->flash["note"] = "Serie ".$preference->series->title." rimossa dai preferiti";
			}else{
				$this->flash["errors"] = $preference->errors;
			}
		}
		$this->redirectTo("referer");
	}

	function newsletter_on($preference_id) {
		$this->update_newsletter($preference_id, '1');
		$this->redirectTo("referer");
	}
	function newsletter_off($preference_id) {
		$this->update_newsletter($preference_id, '0');
		$this->redirectTo("referer");
	}
	private function update_newsletter($preference_id,$val) {
		$preference = Preference::find_first_by_id($preference_id);
		$preference= $this->session->user->has_preference($preference);
		if($preference){
			$preference->newsletter = $val;
			if($preference->update() && $this->update_preference()){
				$this->flash["note"] = "Aggiornata iscrizione alla newsletter con successo";
			}else{
				$this->flash["errors"] = $preference->errors;
			}
		}
	}
	private function update_preference() {
		$this->session->user->preferences= Preference::find(array("id_user"=>$this->session->user->id));
		return true;
	}
}
?>