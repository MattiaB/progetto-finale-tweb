<?php
class SeriesController extends ApplicationController{
	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array(
		"modifyAuth"=>array("modify"),
		"userAuth"=>array("create","create_with_wiki","own_series"),
		"bloggerAuth"=>array("delete","active","active_all","create_with_wiki","deActive"));
		require 'series_helper.php';
	}
	protected  function modifyAuth() {
		$args = func_get_args();
		$series_id = array_shift($args[2]);
		$series = Series::find_first_by_id($series_id);
		if($this->isUserOrMore($series)){
			return true;
		}else{
			$this->flash["errors"] = "Accesso non permesso";
			$this->redirectTo("referer");
		}
	}
	function index($seriesId = null, $page = null) {
		$view = null;
		if(intval($seriesId)){
			$this->series = Series::find_first_by_id($seriesId);
			if(!$this->isUserOrMore($this->series)){
				if($this->series->active != '1'){
					$this->series = null;
				}
			}
			$view = "series";
		}else{
			$this->series =Series::find_with_page($page,$num_pages,$count_result,array('active'=>'1'),null,5);
			$this->page = $page;
			$this->num_pages = $num_pages;
		}
		$this->view($view);
	}
	function create_with_wiki() {
		$this->title ="Crea serie";
		$this->url = array("series","create_with_wiki");
		$this->series = new Series();
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$note []= array();
			if($this->post->active){
				Wiki::$series_array_default = array("active"=>1);
				Wiki::$season_array_default = array("active"=>1);
				Wiki::$episode_array_default= array("active"=>1);
			}
			$links = explode(",", $this->post->link);
			foreach ($links as $link) {
				if($link != '')
				$series = Wiki::new_parse_series($link,$this->session->user);
				if($series && $series->save(false)){
					$note []= "E' stata aggiunta questa serie: ".$series->title;
				}
			}
			$this->flash["note"] = $note;
			$this->redirectTo(array("series"));
		}
	}
	function create() {
		$this->title ="Crea serie";
		$this->url = array("series","create");
		$this->series = new Series();
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$series = new Series();
			$this->fixes_post_data('genres','Genre');
			$this->fixes_post_data('actors','Actor');
			$series->setData($this->post);
			$series->user = $this->session->user;
			$series->uploadImage();
			if($series->save()){
				$this->flash["note"] = "Serie creata";
				$this->redirectTo(array("series",$series->id));
			}else{
				$this->flash["errors"]=$series->errors;
				$this->series = $series;
				$this->view();
			}
		}
	}
	private function fixes_post_data($field,$class) {
		$ris= array();
		foreach ((array)$this->post->$field as $value) {
			$ris []= $class::find_first_by_id($value);
		}
		$this->post->$field = $ris;
	}
	function delete($series_id) {
		$series = Series::find_first_by_id($series_id);
		if($series){
			foreach ($series->seasons as $season) {
				$season->delete();
			}
			foreach ($series->episodes as $episode) {
				$episode->delete();
			}
			foreach ($series->preferences as $pref) {
				$pref->delete();
			}
			$series->delete();
			$this->flash["note"] = "Serie è stata cancellata";
		}else{
			$this->flash["errors"] = "Serie non esiste";
		}
		$this->redirectTo("referer");
	}
	function modify($series_id) {
		$this->url = array("series","modify",$series_id);
		$this->series = Series::find_first_by_id($series_id);
		$this->title ="Modifica la serie";
		$this->series->inverse_format_date('date_start');
		$this->series->inverse_format_date('date_end');
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->post->id =$series_id;
			$this->fixes_post_data('genres','Genre');
			$this->fixes_post_data('actors','Actor');
			$path_img = $this->series->path_img;
			$this->series->setData($this->post);
			$this->series->path_img = $path_img;
			if($this->series->uploadImage() && $this->series->update()){
				$this->flash["note"] = "Serie modificata";
				$this->redirectTo(array("series",$series_id));
			}else{
				$this->series->inverse_format_date('date_start');
				$this->series->inverse_format_date('date_end');
				$this->flash["errors"]=$this->series->errors;
				$this->view();
			}
		}
	}
	function active($series_id) {
	$this->genericActive($series_id,'Series','Attiva','1');
	}
	function deActive($series_id) {
		$this->genericActive($series_id,'Series','Disattiva','0');
	}
	function active_all() {
		$seriess = Series::find(array("active"=>0));
		foreach ($seriess as $series) {
			$series->active='1';
			$series->update();
		}
		$this->redirectTo("referer");
	}

	function own_series($page = null) {
		$cond = array();
		$string = "FIELD (id_user, ".$this->session->user->id.") DESC, created_at DESC";
		$opt = array("order"=>$string);
		if(!$this->get->isEmpty()){
			$search = $this->get->generic;
			Record::safe($search);
			$opt += array("where"=>"title LIKE '%$search%' ");
		}
		if(!$this->isBloggerOrMore()){
			$cond +=array("id_user"=>$this->session->user->id);
		}
		$this->seriess = Series::find_with_page($page,$num_pages,$count_result,$cond,$opt);
		$this->page = $page;
		$this->num_pages = $num_pages;
		$this->view();
	}
}
/*
 * elseif($seriesId != ''){
$cond = array();
$key= '';
$i=0;
foreach (func_get_args() as $value) {
if($i%2==0) $key= $value;
if($i%2==1) $cond[$key]= urldecode($value);
$i++;
}
$this->series = Series::find_complex_cond($cond);
}else{
$this->series =Series::find_active();
}
if($this->series)
$this->view($view);
else
$this->redirectTo("index");
*/
