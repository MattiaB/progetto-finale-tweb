<?php
class SearchController extends ApplicationController{
	private $series_ids = array();
	public function ajaxSearch() {
		$this->data = $this->get;

		$this->count_seriess = 0;
		$this->series_ids=array();
		$this->searchGenericString();
		$this->find($this->series_ids, 'Series');
		$ris = array();
		foreach ((array)$this->seriess as $value) {
			$aux = $value->getArray();
			$ris []= $aux['title'];
		}
		echo json_encode($ris);
	}
	public function index($page = null) {
		if(!$this->get->isEmpty()){
			$num_for_page= 8;
			$this->count_seriess = 0;$this->count_episodes= 0;
			$this->count_genres= 0;$this->count_actors= 0;$this->count_channels= 0;
			$this->data = $this->get;
			$this->seriess = array();
			$this->searchGenericString();
			$this->searchSeries($this->data->genres,"GenresSeries","id_genre");
			$this->searchSeries($this->data->actors,"ActorsSeries","id_actor");
			$this->searchSeries($this->data->channels,"Series","id_channel","id");
			$this->vote();
			$this->continuously();
			$this->order();
			$this->findEpisode();
			$this->find($this->data->genres, 'Genre');
			$this->find($this->data->actors, 'Actor');
			$this->find($this->data->channels, 'Channel');
			$this->page = $page;
			$count_series = count($this->series_ids);
			$count_ep = $this->count_episodes;
			$count = $count_ep + $count_series ;
			$this->num_pages = intval($count / $num_for_page);
			$down_limit = intval($page) * $num_for_page;
			if($count_series < $down_limit){
				$down_limit = $down_limit-$count_series;
				if($this->episodes){
					$ep = $this->episodes;
					$this->episodes = array_splice($ep,$down_limit-1,$num_for_page);
				}
			}else{
				if($this->series_ids)  $this->series_ids = array_splice($this->series_ids,$down_limit,$num_for_page);
				$this->find($this->series_ids, 'Series');
				
				if($down_limit+$num_for_page > $count_series ){
					$num_for_page = $num_for_page-($count_series-$down_limit);

					$ep = $this->episodes;
					if($ep) $this->episodes = array_splice($ep,0,$num_for_page-1);
				}else{
					$this->episodes = null;
				}
			}
			$this->view();
		}else {
			$this->view("form");
		}
	}
	private function order() {
		$order = $this->data->order;
		$this->s_opt = array("order"=>"title ASC");
		$this->e_opt = array("order"=>"title ASC");
		if($order){
			$type = 'ASC';
			if(substr($order, 0,1) == '!' ){
				$type = 'DESC';
				$order = substr($order, 1);
			}
			$order = Record::safe($order);
			$this->s_opt = array("order"=>"$order $type");
			$this->e_opt = array("order"=>"$order $type");
		}
	}
	private function find($array_id, $class,$field='id', $opt=null,$return=null){
		if($array_id){
			$queryData = implode(",",$array_id);
			Record::safe($queryData);
			$new_opt = array("where"=>"`$field` IN ($queryData)");
			if($opt){
				if(isset($opt['where'])){
					$new_opt['where'] .= $opt['where'];
				}
				$new_opt =  $new_opt + (array)$opt;
			}
			$ris =  $class::find(null,$new_opt);
			if(!$return){
				$var = strtolower($class).'s';
				$this->$var = $ris;
				$var = "count_$var";
				$this->$var = $class::$last_query_count;
			}else{
				return $ris;
			}
		}
		return array();
	}

	private function findEpisode() {
		$this->count_episodes=0;
		$string = Series::safe($this->data->generic);
		$cond = array();
		if($string){
				$num_matches = preg_match('/(.*?)[\s][s]?(\d*)[\sxe](\d*)/',$string,$matches);
			if($num_matches){
				$series = trim($matches[1]);
				$season = intval($matches[2]);
				$episodes = intval($matches[3]);
				//$series = Series::find(null,array("first"=>true,"where"=>"MATCH(title) AGAINST ('*$series*' IN BOOLEAN MODE)"));
				$series = Series::find(null,array("first"=>true,"where"=>" title LIKE '%$series%' "));
				if($series){
					$season = Season::find(array("id_series"=>$series->id,"number_season"=>$season),array("first"=>true));
					if($season)
					$cond = array("id_series"=>$series->id,"id_season"=>$season->id,"no_in_season"=>$episodes);
				}
			}else{
				//$this->e_opt = (array)$this->e_opt + array("where"=>"MATCH(title) AGAINST ('*$string*' IN BOOLEAN MODE)",'order'=>'id_series ASC');
				$this->e_opt = (array)$this->e_opt + array("where"=>" title LIKE '%$string%' ",'order'=>'id_series ASC');
			}
			$this->episodes = Episode::find($cond,$this->e_opt);
			$this->count_episodes = Episode::$last_query_count;
			if($num_matches && $this->count_episodes ) $this->series_ids=array();
		}
	}

	private function continuously() {
		$seriesId = array();
		$continuously = (array)$this->data->continuously;
		if(in_array("end",$continuously))
		$seriesId += Series::find(null,array("where"=>"date_end IS NOT NULL","onlyIds"=>true));
		if(in_array("now",$continuously))
		$seriesId += Series::find(null,array("where"=>"date_end IS NULL","onlyIds"=>true));
		$this->intersect($seriesId);
	}
	private function vote() {
		$votes = $this->data->votes;
		$seriesId = array();
		foreach ((array)$votes as $vote) {
			$seriesId += Series::find(null,array("where"=>"rate >= $vote AND rate < ".($vote+1),"onlyIds"=>true));
		}
		$this->intersect($seriesId);
	}
	private function searchGenericString() {
		$seriesId = array();
		$string = $this->data->generic;
		$this->search_string = $this->data->generic;
		if($string){
			$generic = Series::safe($string);
		//	$op = array("where"=>"MATCH(title) AGAINST ('*$generic*' IN BOOLEAN MODE)","onlyIds"=>true);
			$op = array("where"=>" title LIKE '%$generic%' ","onlyIds"=>true);
			$seriesId = Series::find(null,$op);

		//	$op = array("where"=>"MATCH(name) AGAINST ('*$generic*' IN BOOLEAN MODE)","onlyIds"=>true);
			$op = array("where"=>" name LIKE '%$generic%' ","onlyIds"=>true);

			$this->data->genres = (array)$this->data->genres + Genre::find(null,$op);
			$this->data->actors = (array)$this->data->actors  + Actor::find(null,$op);
			$this->data->channels = (array)$this->data->channels  + Channel::find(null,$op);

			$this->searchSeries($this->data->genres,"GenresSeries",'id',"id_genre",'union');//Guardare se il 'id'è corretto
			$this->searchSeries($this->data->actors,"ActorsSeries",'id',"id_actor",'union');
			$this->searchSeries($this->data->channels,"Series",'id',"id_channel",'union');

		}
		$this->intersect($seriesId);
	}
	private function searchSeries($data, $class, $field ,$group = "id_series", $type = 'intersect') {
		$seriesId = $this->find($data, $class,$field,array("where"=>" GROUP BY `$group`","onlyIds"=>$group),true);
		$this->$type($seriesId);
	}
	private function intersect($ids) {
		if(!empty($ids)){
			if(!empty($this->series_ids)){
				$this->series_ids = array_intersect($this->series_ids,$ids);
			}
			else{
				$this->series_ids = $ids;
			}
		}
	}
	private function union($ids) {
		if(!empty($ids)){
			if(!empty($this->series_ids)){
				$this->series_ids = $this->series_ids + $ids;
			}
			else{
				$this->series_ids = $ids;
			}
		}
	}
}
