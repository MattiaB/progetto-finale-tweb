<?php
class SessionsController extends ApplicationController{

	function index() {
		$this->title = "Login";
		$this->view();
	}
	function create() {
		if(!$this->isUserOrMore()){
			if(!$this->post->isEmpty()){
				$user =User::authenticate($this->post->email,$this->post->password);
				if(!$user){
					$this->flash['errors'] = "Non è stato possibile autenticare l'utente";
					$this->redirectTo("referer");
				}else{
					$this->flash['note'] = 'Log in avvenuto con successo';
					$this->session->user = $user;
					$this->redirectTo("referer");
				}
			}else{
				$this->flash['errors'] = "Non è stato possibile autenticare l'utente";
				$this->redirectTo(array("sessions"));
			}
		}else{
			$this->redirectTo(array("users"));
		}
	}

	function destroy() {
		$this->session->destroy();
		$this->flash['note'] = 'Log out avvenuto con successo';
		$this->redirectTo("index");
	}

}