<?php
class ActorController extends ApplicationController{
	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array("bloggerAuth"=>
		array("modify","delete"));
	}
	function index($page = null) {
		$opt = array("order"=>"name ASC");
		$cond = array();
		if(!$this->get->isEmpty()){
			$search = $this->get->generic;
			Record::safe($search);
			$opt += array("where"=>"name LIKE '%$search%' ");
		}
		$this->actors = Actor::find_with_page($page,$num_pages,$count_result,$cond,$opt);
		$this->page = $page;
		$this->num_pages = $num_pages;
		$this->view();
	}
	function modify($actor_id) {
		$this->actor = Actor::find_first_by_id($actor_id);
		if($this->actor){
			$this->url = array("actor","modify",$this->actor->id);
			$this->title = "Modifica l'attore: ".$this->actor->name;
			if($this->post->isEmpty()){
				$this->view();
			}else{
				$this->actor->setData($this->post);
				if($this->actor->update()){
					$this->flash["note"] = "Attore ".$this->actor->name." modificato";
					$this->redirectTo(array("actor"));
				}else{
					$this->flash["errors"]=$this->episode->errors;
					$this->view();
				}
			}
		}else{
			$this->flash["errors"]="L'attore non esiste";
			$this->redirectTo("referer");
		}
	}
	function create() {
		$this->title ="Crea Attore";
		$this->url = array("actor","create");
		$this->actor = new Actor();
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->actor->setData($this->post);
			if($this->actor->save()){
				$this->flash["note"] = "Attore ".$this->actor->name.' creato';
				$this->redirectTo(array("actor"));
			}else{
				$this->flash["errors"]=$this->actor->errors;
				$this->view();
			}
		}
	}
	function delete($actor_id) {
		$this->actor = Actor::find_first_by_id($actor_id);
		if($this->actor){
			if($this->actor->delete()){
				$this->flash["note"] = "L'attore è stato ".$this->actor->name." cancellato";
			}else{
				$this->flash["errors"]=$this->episode->errors;
			}
			$this->redirectTo('referer');
		}else{
			$this->flash["errors"]="L'attore non esiste";
			$this->redirectTo('referer');
		}
	}
}