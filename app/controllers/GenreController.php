<?php
class GenreController extends ApplicationController{
	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array("bloggerAuth"=>
		array("modify","delete"));
	}
	function index($page = null) {
		$opt = array("order"=>"name ASC");
		$cond = array();
		if(!$this->get->isEmpty()){
			$search = $this->get->generic;
			Record::safe($search);
			$opt += array("where"=>"name LIKE '%$search%' ");
		}
		$this->genres = Genre::find_with_page($page,$num_pages,$count_result,$cond,$opt);
		$this->page = $page;
		$this->num_pages = $num_pages;
		$this->view();
	}
	function modify($genre_id) {
		$this->genre = Genre::find_first_by_id($genre_id);
		if($this->genre){
			$this->url = array("genre","modify",$this->genre->id);
			$this->title = "Modifica il genere: ".$this->genre->name;
			if($this->post->isEmpty()){
				$this->view();
			}else{
				$this->genre->setData($this->post);
				if($this->genre->update()){
					$this->flash["note"] = "Genere ".$this->genre->name." modificato";
					$this->redirectTo(array("genre"));
				}else{
					$this->flash["errors"]=$this->episode->errors;
					$this->view();
				}
			}
		}else{
			$this->flash["errors"]="L'attore non esiste";
			$this->redirectTo("referer");
		}
	}
	function create() {
		$this->title ="Crea Genere";
		$this->url = array("genre","create");
		$this->genre = new genre();
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->genre->setData($this->post);
			if($this->genre->save()){
				$this->flash["note"] = "Genere ".$this->genre->name.' creato';
				$this->redirectTo(array("genre"));
			}else{
				$this->flash["errors"]=$this->genre->errors;
				$this->view();
			}
		}
	}
	function delete($genre_id) {
		$this->genre = Genre::find_first_by_id($genre_id);
		if($this->genre){
			if($this->genre->delete()){
				$this->flash["note"] = "Il genere è stato ".$this->genre->name." cancellato";
			}else{
				$this->flash["errors"]=$this->episode->errors;
			}
			$this->redirectTo('referer');
		}else{
			$this->flash["errors"]="Il genere non esiste";
			$this->redirectTo('referer');
		}
	}
}
