<?php

class EpisodeController extends ApplicationController{
	function __construct() {
		static::$beforeFilter =	parent::$beforeFilter +
		array(
		"modifyAuth"=>array("modify"),
		"userAuth"=>array("create","vote","own_episodes"),
		"bloggerAuth"=>array("delete","active","active_all"),
		"adminAuth"=>array("create_with_wiki"));
	}
	function modifyAuth() {
		$args = func_get_args();
		$episode_id = array_shift($args[2]);
		$episode = Episode::find_first_by_id($episode_id);
		if($this->isUserOrMore($episode)){
			return true;
		}else{
			$this->redirectTo("referer");
		}
	}
	function index($episode_id) {
		$this->episode = Episode::find_first_by_id($episode_id);
		if(!$this->isUserOrMore($this->episode)){
				if($this->episode->active != '1'){
					$this->episode = null;
				}
			}
		if($this->episode){
			$this->view();
		}
		else{
			$this->flash['errors'] = "L'episodio non esiste";
			$this->redirectTo("referer");
		}
	
	}

	function create_with_wiki($series_id,$season_id) {
		$this->series = Series::find_first_by_id($series_id);
		$this->season = Seasion::find_first_by_id($season_id);
		$this->episode = new Episode();
		$this->url = array("episode","create_with_wiki",$this->series->id,$this->season->id);
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$episodes= Wiki::parse_more_season_episodes($this->post->link,$this->series);
		}
	}
	function create($series_id,$season_id) {
		$this->url = array("episode","create");
		$this->episode = new Episode();
		$this->episode->series = Series::find_first_by_id($series_id);
		$this->episode->season = Season::find_first_by_id($season_id);
		$this->episode->user =  $this->session->user;
		$this->url = array("episode","create",$this->episode->series->id,$this->episode->season->id);
		$this->title = "Crea un episodio";
		if($this->post->isEmpty()){
			$this->view();
		}else{
			$this->episode->setData($this->post);
			var_dump($this->episode);
			if($this->episode->save()){
				$this->flash["note"] = "Episodio ".$this->episode->title." creato";
				$this->redirectTo(array("episode",$this->episode->id));
			}else{
				$this->flash["errors"]=$this->episode->errors;
				$this->view();
			}
		}
	}

	function modify($episode_id) {
		$this->episode = Episode::find_first_by_id($episode_id);
		if($this->episode){
			$this->url = array("episode","modify",$this->episode->id);
			$this->title = "Modifica l'episodio: ".$this->episode->title;
			if($this->post->isEmpty()){
				$this->view();
			}else{
				$this->episode->setData($this->post);
				if($this->episode->update()){
					$this->flash["note"] = "Episodio ".$this->episode->title." modificato";
					$this->redirectTo(array("episode",$this->episode->id));
				}else{
					$this->flash["errors"]=$this->episode->errors;
					$this->view();
				}
			}
		}else{
			$this->flash["errors"]="L' episodio non esiste";
			$this->redirectTo("referer");
		}
	}
	function delete($episode_id) {
		$this->episode = Episode::find_first_by_id($episode_id);
		if($this->episode){
			if($this->episode->delete()){
				$this->flash["note"] = "Episodio \"".$this->episode->title."\" cancellato";
				$this->redirectTo('referer');
			}else{
				$this->flash["errors"]=$this->episode->errors;
				$this->redirectTo('referer');
			}
		}else{
			$this->flash["errors"]="L'episodio non esiste";
			$this->redirectTo("index");
		}
	}
	function active($episode_id) {
		$this->genericActive($episode_id,'Episode','Attiva','1');
	}
	function deActive($episode_id) {
		$this->genericActive($episode_id,'Episode','Disattiva','0');
	}
	function active_all() {
		//$season = Seasion::find_first_by_id($season_id);
		$episodes = Episode::find(array("active"=>0));
		foreach ($episodes as $episode) {
			$episode->active='1';
			$episode->update();
		}
		$this->redirectTo("referer");
	}
	function own_episodes($page = null) {
		$opt = array("order"=>"created_at DESC, no_in_series ASC");
		$cond = array();
		if(!$this->get->isEmpty()){
			$search = $this->get->generic;
			Record::safe($search);
			$opt_s = array("where"=>"title LIKE '%$search%' ","onlyIds"=>true);
			$cond_s = array();
			$series_ids = Series::find($cond_s,$opt_s);
			if($series_ids){
				$seriesId = implode(",",$series_ids);
				if(!$this->isBloggerOrMore()){
					$opt += array("where"=>"AND (`id_series` IN($seriesId) OR `title` LIKE '%$search%') ");
				}else{
					$opt += array("where"=>"(`id_series` IN($seriesId) OR `title` LIKE '%$search%') ");
				}
			}
		}
		if(!$this->isBloggerOrMore()){
			$cond += array("id_user"=>$this->session->user->id);
		}
		$this->episodes = Episode::find_with_page($page,$num_pages,$count_result,$cond,$opt);
		$this->page = $page;
		$this->num_pages = $num_pages;
		$this->view();
	}
}

?>