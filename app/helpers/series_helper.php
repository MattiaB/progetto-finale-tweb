<?php
function series_date($series) {
	$original_run = strDate($series->date_start)." - ";
	if(!$series->date_end){
		$original_run .= 'Adesso';
	}else{
		$original_run .= strDate($series->date_end);
	}
	return $original_run;
}
function series_genres($series) {
	$genres='';
	foreach ((array)$series->genres as $genre) {
		$http_query = Request::static_to_http_query(array("genres[]"=>$genre->id));
		$genres .= link_to(array("search","add"=>$http_query),safe($genre->name));
	}
	return $genres;
}
function series_channel($series) {
	if($series->channel){
		$http_query = Request::static_to_http_query(array("channels[]"=>	$series->channel->id));
		return link_to(array("search","add"=>$http_query),safe($series->channel->name));
	}else{
		return "";
	}
}
function series_actors($series) {
	$actors='';
	foreach ((array)$series->actors as $actor) {
		$http_query = Request::static_to_http_query(array("actors[]"=>$actor->id));
		$actors .= link_to(array("search","add"=>$http_query), safe($actor->name));
	}
	return $actors;
}
