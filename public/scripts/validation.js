function numeric(obj) {
	if ($(obj).val() != '') {
		return jQuery.isNumeric($(obj).val());
	}
	return true;
}
function email(obj) {
	var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;
	return reg.test($(obj).val())
}
function minlength(obj) {
	if ($(obj).val() != '') {
		return $(obj).val().length > $(obj).attr('length');
	}
	return true;

}
function maxlength(obj) {
	if ($(obj).val() != '') {
		return $(obj).val().length < $(obj).attr('length');
	}
	return true;
}
function alphanumeric(obj) {
	var reg = /^(\w*)$/;
	return reg.test($(obj).val())
}
function date(obj) {
	if ($(obj).val() != '') {
		var reg = new RegExp($(obj).attr('regexp'));
		return reg.test($(obj).val());
	} else
		return true;
}
function notempty(obj) {
	return $(obj).val() != '';
}
function addErrors(string, className, typeofValidation) {
	if (!$('#content > .errors > .' + className + '.' + typeofValidation).length) {
		var p = $('<p></p>').addClass(className).addClass(typeofValidation)
				.text(string);
		if (!$('#content > .errors').length) {
			var div = $('<div></div>').addClass('errors');
			$('#content').prepend(div);
		}
		$('#content > .errors').append(p);
	}
}
function removeErrors(className, typeofValidation) {
	if ($('#content > .errors').length) {
		$('#content > .errors > .' + className + '.' + typeofValidation)
				.remove();
		if (!$('#content > .errors p').length) {
			$('#content > .errors').remove();
		}
	}
}
(function($) {
	$.fn.valid = function() {
		var ris = true;
		var errorClass = "errors";
		var funcs = new Array('numeric', 'email', 'minlength', 'maxlength',
				'alphanumeric', 'date', 'notempty');
		for ( var int = 0; int < funcs.length; int++) {
			if ($(this).hasClass(funcs[int])) {
				ris = window[funcs[int]](this);
				var name = $(this).attr('name');
				if (!ris) {
					$(this).addClass(errorClass);
					addErrors($(this).attr(funcs[int]), name, funcs[int]);
				} else {
					removeErrors($(this).attr('name'), funcs[int]);
					if (!$('#content > .errors>.' + name).length)
						$(this).removeClass(errorClass);

				}
			}
		}
		return ris;
	};
})(jQuery);
function validation() {
	$(".form").submit(function() {
		var ris = true;
		$(this).find('input').each(function() {
			if ($(this).attr('value') == $(this).attr('title')) {
				$(this).attr('value', '');
			}
			ris = ris && $(this).valid();
		});
		return ris;
	});
	$(".form input").focusout(function() {
		if ($(this).attr('value') != $(this).attr('title')) {
			$(this).valid();
		}
	});
}
$(window).load(function() {
	validation();
});