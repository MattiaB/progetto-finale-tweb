function search() {

	$(".filter > input").keyup(function() {
		var filter = $(this).val()
		$(this).parent().find(".boxcheckbox>div").each(function() {
			if ($(this).text().search(new RegExp(filter, "i")) < 0) {
				$(this).addClass("hidden");
			} else {
				$(this).removeClass("hidden");
			}
		});
	});
	$(".form input[title]:not(.textSearch)").each(function() {
		if ($(this).attr('value') == '') {
			$(this).attr('value', $(this).attr('title'));
			$(this).addClass('grey');
		}
	});
	$(".form input[title]:not(.textSearch)").focus(function() {
		if ($(this).attr('value') == $(this).attr('title')) {
			$(this).attr('value', '');
			$(this).removeClass('grey');
		}
	}).focusout(function() {
		if ($(this).attr('value') == '') {
			$(this).attr('value', $(this).attr('title'));
			$(this).addClass('grey');
		}
	});
	$(".form").submit(function() {
		$('.textSearch').attr('title','');
		$(".form input[title]").each(function() {
			if ($(this).attr('value') == $(this).attr('title')) {
				$(this).attr('value', '');
			}
		});
	});
	$(".form input[type=checkbox] ~ span,.form input[type=radio] ~ span").each(
			function() {
				$(this).css('cursor', 'pointer');
			});
	$(
			".form input[type=checkbox],.form input[type=checkbox] ~ span,.form input[type=radio] ~ span")
			.click(
					function() {
						var c = $(this).prev().is(':checked');
						$(this).prev().attr('checked', !c);
						var text = $(this).parent().parent().parent().find(
								'input[type=text]');
						if ($(text).attr('value') != $(text).attr('title')) {
							$(text).attr('value', '').focus();
						}
					});
}
function removeSpeedSearch(input) {
	$(input).parent().find('.speedSearch').hide();

}
function clickSpan(span) {
	val = $(span).text();
	var input = $(span).parent().parent().find('.textSearch');
	$(input).val(val);
	$(input).focus();
	removeSpeedSearch(input);
}
function getSearch(input, val) {
	var link = $(input).parents('.form').attr('action');
	$.get(link + '/ajaxSearch', {
		generic : val
	}, function(data) {
		try {
			var obj = $.parseJSON(data);
			if (obj.length) {
				$(input).parent().addClass('relative');
				var div = $(input).parent().find('.speedSearch');
				if (!div.length) {
					div = $('<div class="speedSearch"></div>');
				}
				div.show();
				div.html('');
				$(input).after(div);
				for ( var int = 0; int < obj.length; int++) {
					var elem = obj[int];
					$(div).append('<span>' + elem + '</span>')
				}
				var p = $(div).find('span');
				$(div).find('span').mousedown(function(event) {
					clickSpan(this);
					event.preventDefault();
				});
				$(div).find('span').hover(function() {
					$(this).parent().find('.selectedString').removeClass();
					$(this).addClass('selectedString');
				}, function() {
					$(this).removeClass('selectedString');
				});
			} else {
				removeSpeedSearch(input);
			}
		} catch (e) {
			$('#content').html(data);
		}
	});
}

function aggSearch(input, val) {
	if (val != '') {
		getSearch(input, val.toLowerCase());
	} else {
		removeSpeedSearch(input);
	}
}
function savetext(input, val) {
	$(input).attr('title', val);
}
function speedSearch() {
	$('.textSearch').keyup(function(event) {

		if(!(event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 40 || event.keyCode == 38)){
			val = $(this).val();
			aggSearch(this, val);
			$(this).attr('title', val);
		}
	});
	$('.textSearch').keydown(
			function(event) {
				var val;
				switch (event.keyCode) {
				case 8:
					val = $(this).val().substr(0, $(this).val().length - 1);
					aggSearch(this, val)
					break;
				case 9:
					if ($(this).parent().find('.speedSearch')) {
						val = $(this).parent().find('	.speedSearch span:first')
								.text();
						$(this).val(val);
					}
					break;
				case 40:
					var elems = $(this).parent().find('.speedSearch');
					if (elems.length) {
						if ($(elems).find('.selectedString').length) {
							val = $(elems).find('.selectedString').removeClass(
									'selectedString').next().addClass(
									'selectedString').text();
						} else {
							val = $(elems).find('span:first').addClass(
									'selectedString').text();
						}
						if (!val.length) {
							val = $(this).attr('title');
						}
						$(this).val(val);
						event.preventDefault();
					}
					break;
				case 38:
					var elems = $(this).parent().find('.speedSearch');
					if (elems.length) {
						if ($(elems).find('.selectedString').length) {
							val = $(elems).find('.selectedString').removeClass(
									'selectedString').prev().addClass(
									'selectedString').text();
						} else {
							val = $(elems).find('span:last').addClass(
									'selectedString').text();
						}
						if (!val.length) {
							val = $(this).attr('title');
						}
						$(this).val(val);
					}
					break;
				default:

					break;
				}
			});

	$('.textSearch').focusout(function() {
		removeSpeedSearch(this);
		
	});
	$('.textSearch').focusin(function() {
		//getSearch(this, $(this).val());
	});
}
$(window).load(function() {
	search();
	speedSearch();
});