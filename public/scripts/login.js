$(window).load(function() {
	$('.slidelogin .holding>input').each(function() {
		if ($(this).val() != "") {
			if (!$(this).parent().hasClass("hasome"))
				$(this).parent().addClass("hasome");
		}
	});
	$('.slidelogin .holding>span').click(function() {
		$(this).parent().find('input').focus();
	});
	$('.slidelogin .holding>input').keypress(function() {
		if (!$(this).parent().hasClass("hasome"))
			$(this).parent().addClass("hasome");
	});
	$('.slidelogin .holding>input').focusout(function() {
		if ($(this).val() == "") {
			if ($(this).parent().hasClass("hasome"))
				$(this).parent().removeClass("hasome");
		}
	});
	indexImg();
});
function indexImg() {
	$('.indexdivImg').hover(function() {
		$(this).find('span').css('display', 'block');
	}, function() {
		$(this).find('span').css('display', 'none');
	});
}