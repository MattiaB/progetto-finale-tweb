
function selectedlink(href) {
	$('.selected').each(function() {
		$(this).removeClass('selected');
	});
	$('a').each(function name() {
		if(href == $(this).attr('href')){
			$(this).addClass('selected');
		}
	})
	
}
function classicLink(href) {
	selectedlink(href);
	$('#content').addClass('relative');
	$('#content').fadeTo('fast', 0.3, function() {
		$('#content').append('<div class="overlay"></div>');
		$('html, body').animate({ scrollTop: 40 }, 'fast');
		$.get(href, function(data) {
			//window.history.pushState('', document.title, href);
			$('#content').removeClass('relative');
			$('#content').html(data).fadeTo('fast', 1, function() {
				$('#content a').click(function() {
					return ajaxlink(this);
				});
				validation();
				search();
				indexImg();
				speedSearch();
			});
		});
	});
	return false;
}
function ajaxdeletelink(href, link) {
	$.get(href, function(data) {
		$('#content .message').fadeOut('fast').remove();
		$('#content').prepend($(data).hide());
		$('#content .message').fadeIn('slow');
		$(link).parent().parent().slideUp('slow', function() {
			$(this).remove();
		});
	});
	return false;

}
var lasturl=""; //here we store the current URL hash
function ajaxlink(link) {
	if ($(link).hasClass('noajax')) {
		return true;
	}
	var href = $(link).attr('href');
	if ($(link).hasClass('ajaxdeletelink')) {
		return ajaxdeletelink(href, link);
	}
	window.history.pushState('', document.title, href);
	return false;

}
$(document).ready(function() {
	$('a').click(function() {
		ris = ajaxlink(this);
		return ris;
	});
    lasturl =window.location.href;
    setInterval("checkURL()",250);  
});
function loadPage(href) {
		classicLink(href);
}

function checkURL(hash)
{
    if(!hash) hash=window.location.href;    //if no parameter is provided, use the hash value from the current address
    if(hash != lasturl) // if the hash value has changed
    {
        lasturl=hash;   //update the current hash
        loadPage(hash); // and load the new page
    }

}
