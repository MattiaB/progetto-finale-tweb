<?php
function urlPath($relPath="") {
	$path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$aux = strstr($path,"dbconfig.php",true);
	if($aux) $path = $aux;
	$aux = strstr($path,"index.php",true);
	if($aux) $path = $aux;
	return "http://$path".$relPath;
}

function initDB($config) {
	$templine = '';
	$lines = @file($config['mysqlFileDb']);
	if($lines){
		foreach ($lines as $line){
			if (substr($line, 0, 2) == '--' || $line == '')
			continue;
			$templine .= $line;
			if (substr(trim($line), -1, 1) == ';'){
				mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
				$templine = '';
			}
		}
	}else throw new Exception("Il file ".$config['mysqlFileDb'].' non si può aprire');
}
if (isset($exc)) {
	if ($exc->getCode() == 2) {
		if (mysql_query('CREATE DATABASE ' . $config['mysqlDb'])) {
			mysql_select_db($config['mysqlDb']);
			initDB($config);
		}else
		throw new Exception("Non si puo' creare il database, crearlo o cambiare nome nel file app/config.php");
	}
	if ($exc->getCode() == 3) {
		initDB($config);
	}
	if ($exc->getCode() == 1) {
		?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Configurazione dabase</title>
<link rel="stylesheet" type="text/css"
	href="<?php echo urlPath("public/css/layout.css") ?>" />
<link rel="stylesheet" type="text/css"
	href="<?php echo urlPath("public/css/design.css") ?>" />
</head>
<body>
	<div id="content">
		<h3>Configurazione dati database</h3>
		<form action="<?php echo urlPath('dbconfig.php') ?>" method="post"
			class="form">
			<fieldset>
				<legend>Dati</legend>
				<label>Host:*</label><input type="text" name="host" value="<?php echo $config["mysqlHost"]?>" /> <label>User*:</label><input
					type="text" name="user"  value="<?php echo $config["mysqlUser"]?>" /> <label>Password*:</label><input
					type="text" name="password"  value="<?php echo $config["mysqlPsw"]?>" /> <label>Nome Database*:</label><input
					type="text" name="db"  value="<?php echo $config["mysqlDb"]?>"  />
			</fieldset>
			<?php echo formSubmit('Config Database')?>
		</form>
	</div>
</body>
</html>

<?php
exit;
	}
} else
if (isset($_POST["host"]) && isset($_POST["user"]) && isset($_POST["password"])) {
	if (!isset($config)){
		require 'app/config.php';
	}
	if (!is_writable("app/config.php")) {
		throw new Exception("Errore non si hanno i permessi per scrivere sul file app/config.php");
	}
	$config["mysqlHost"] = $_POST["host"];
	$config["mysqlUser"] = $_POST["user"];
	$config["mysqlPsw"]  = $_POST["password"];
	$config["mysqlDb"]  = $_POST["db"];
	$config["urlPath"]	 = strstr($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],"dbconfig.php",true);
	$stringFile = "<?php\n";
	foreach ($config as $key => $value) {
		$stringFile .= '$config["' . $key . '"]="' . $value . '";' . "\n";

	}
	$stringFile .= "\n?>";
	file_put_contents("app/config.php", $stringFile);

}
$url = urlPath();
header("Location: $url");
exit;
?>
